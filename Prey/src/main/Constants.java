package main;

import lejos.nxt.MotorPort;

public class Constants {
	public static final int START_HUNGER = 50;
	public static final int START_ENERGY = 50;
	
	public static final MotorPort LEFT_MOTOR = MotorPort.B;
	public static final MotorPort RIGHT_MOTOR = MotorPort.A;
	public static final MotorPort HEAD_MOTOR = MotorPort.C;
	public static final int MAX_MOTIVATION = 100;
	public static final int RUNNING_MOTIVATION = 50;
	
}
