package main;

import helperclasses.Signal.RobotSignal;
import lejos.nxt.SensorPort;
import lejos.nxt.TouchSensor;
import lejos.nxt.UltrasonicSensor;
import lejos.nxt.addon.IRSeekerV2;
import lejos.nxt.addon.IRSeekerV2.Mode;
import main.Enum.Color;
import main.Enum.GameState;

/**
 * @author HS
 *
 */
public class Sensors extends Thread {

	private IRSeekerV2 infraredSeeker = new IRSeekerV2(SensorPort.S1, Mode.AC);
	private ColorSensor colorSensor = new ColorSensor(SensorPort.S2);
	private UltrasonicSensor ultrasonicSensor = new UltrasonicSensor(SensorPort.S4);
	private TouchSensor touchSensor = new TouchSensor(SensorPort.S3);

	private static Color colorSensorValue = Color.NONE;
	private static volatile int ultraSonicDistance = 255;
	private static int infraredDirection = 0;
	private static int[] infraredValues = null;
	private static int infraredAvrage = 0;
	private static boolean touchSensorValue = false;
	private static final int infraredMeasureMargin = 10;


	/**
	 * 
	 */
	public Sensors() {
		this.setDaemon(true);
		this.start();
		new UltraSonicSensorThread(ultrasonicSensor);
	}

	/**
	 * @return
	 */
	public static int getUltraSonicDistance() {
		return ultraSonicDistance;
	}
	
	public static void setUltraSonicDistance(int d) {
		ultraSonicDistance = d;
	}

	/**
	 * @return
	 */
	public static Color getColourSensorValue() {
		return colorSensorValue;
	}

	/**
	 * @return
	 */
	public static int getInfraredDirection() {
		return infraredDirection;
	}

	/**
	 * @return
	 */
	public static int[] getInfraredValues() {
		return infraredValues;
	}

	/**
	 * @return
	 */
	public static int getAverageInfraredValues() {
		return infraredAvrage;
	}

	public static boolean getTouchSensorValue() {
		return touchSensorValue;
	}

	/* (non-Javadoc)
	 * @see java.lang.Thread#run()
	 */
	public void run() {

		while (true) {
			if (MainProgram.getState() == GameState.RUNNING) {
				colorSensorValue = colorSensor.getColor();
				infraredDirection = infraredSeeker.getDirection();
				infraredValues = infraredSeeker.getSensorValues();
				touchSensorValue = touchSensor.isPressed();
				
				int[] sensorValues = infraredValues;
				int sum = 0;
				int numOfActiveSensors = 0;
				for (int i=0; i < sensorValues.length; i++) {
					if (sensorValues[i] > infraredMeasureMargin) {
						sum += sensorValues[i];
						numOfActiveSensors++;
					}
				}
				if (numOfActiveSensors > 0) {
					infraredAvrage = sum/numOfActiveSensors;
				} else {
					infraredAvrage = 0;
				}
			}
			try { Thread.sleep(10); } catch (InterruptedException e) { }
		}
	}
}

class UltraSonicSensorThread extends Thread {
	private UltrasonicSensor ultrasonicSensor;
	
	public UltraSonicSensorThread(UltrasonicSensor ultrasonicSensor) {
		this.ultrasonicSensor = ultrasonicSensor;
		this.setDaemon(true);
		this.start();
	}
	
	@Override
	public void run() {
		while (true) {
			if (MainProgram.getState() == GameState.RUNNING) {
				Sensors.setUltraSonicDistance(ultrasonicSensor.getDistance());
			} else {
				try { Thread.sleep(10); } catch (InterruptedException e) { }
			}
		}
	}
}