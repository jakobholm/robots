package main;

import lejos.nxt.SensorPort;
import lejos.nxt.Sound;
import main.Enum.Color;

public class ColorSensor {
	
	lejos.nxt.ColorSensor sensor;
	private int foodColorCounter = 0;
	private final int foodColorCounterMax = 2;
	private int timeSpan = 0;
	private final int timeSpanMax = 100;

	public ColorSensor(SensorPort port) {
		sensor = new lejos.nxt.ColorSensor(port);
	}

	public Color getColor() {
		lejos.nxt.ColorSensor.Color c = sensor.getRawColor();
//		MainProgram.getCommunication().send(RobotSignal.DEBUG, c.getBackground());
		// Detect Border
		if (inRange(c.getRed(), 0, 350) && 
				inRange(c.getGreen(), 0, 350) && 
				inRange(c.getBlue(), 0, 350)) {
//			Sound.playTone(256, 100, 50);
//			MainProgram.getCommunication().send(RobotSignal.DEBUG, 1);
			return Color.BORDER;
		}
		
		// Detect Food
		if (inRange(c.getRed(), 400, 450) && 
				inRange(c.getGreen(), 475, 525) && 
				inRange(c.getBlue(), 375, 425)) {
//			Sound.playTone(256, 100, 50);
			return Color.ENERGY;
			
//			foodColorCounter++;
//			timeSpan++;
//			if (timeSpan >= timeSpanMax) {
//				timeSpan = 0;
//				foodColorCounter = 0;
//			}
//			
//			if (foodColorCounter >= foodColorCounterMax) {
//				MainProgram.getCommunication().send(RobotSignal.DEBUG, 2);
//				return Color.FOOD;
//			}
			
			//MainProgram.getCommunication().send(RobotSignal.DEBUG, 3);

		} 
		
	
//		MainProgram.getCommunication().send(RobotSignal.DEBUG, 0);
		return Color.NONE;
	}
	
	private boolean inRange(int value, int min, int max) {
		return (min <= value && value <= max);
	}
	
}
