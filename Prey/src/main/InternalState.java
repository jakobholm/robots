package main;

import helperclasses.PreyCommunication;
import main.Enum.GameState;

public class InternalState extends Thread {
	private static int energy = Constants.START_ENERGY; // ranges from 0 to 100, 100 being full energy
	private static int energyCounter = 0;
	private int maxEnergyCount = 30;
	
	private static PreyCommunication communication;

	public InternalState(PreyCommunication communication){
		InternalState.communication = communication;
		this.setDaemon(true);
		this.start();
	}

	public void run() {
		while(true) {
			if (MainProgram.getState() == GameState.RUNNING) {

				// To avoid the energy being decreased to fast, the energyCounter is used.
				// When the energyCounter is equal to the maxEnergyCount the energy is incremented.
				
				energyCounter++;
				if (energyCounter >= maxEnergyCount) {
					decrementEnergy();
				}
			}

			try { Thread.sleep(10); } catch (InterruptedException e) {}
		}
	}

	public static int getEnergy() {
		return energy;
	}

	private static void decrementEnergy() {
		energy = Math.max(0, energy - 1);
		energyCounter = 0;
		communication.sendEnergySignal(energy);
	}

	public static void incrementEnergy() {
		energy = Math.min(100, energy+1);
		energyCounter = 0;
		communication.sendEnergySignal(energy);
	}

	public static void reset() {
		energy = Constants.START_ENERGY;
	}
}
