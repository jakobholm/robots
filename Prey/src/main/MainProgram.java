package main;

import helperclasses.HeadTurner;
import helperclasses.PreyCommunication;
import helperclasses.Signal.PCSignal;
import helperclasses.Signal.RobotSignal;
import helperclasses.SoundPlayer;
import lejos.nxt.Button;
import lejos.nxt.ButtonListener;
import lejos.nxt.LCD;
import main.Enum.Difficulty;
import main.Enum.DrivingSpeed;
import main.Enum.GameState;
import behaviors.AvoidBorder;
import behaviors.AvoidObstacle;
import behaviors.Behavior;
import behaviors.DeadWatcher;
import behaviors.DetectStalling;
import behaviors.GatherEnergy;
import behaviors.RunAway;
import behaviors.Wander;

public class MainProgram implements ButtonListener {
	
	private static volatile GameState state = GameState.NOT_CONNECTED; // Setting state to QUITTING stops the entire program.
	
	private static PreyCommunication communication;

	public static void main(String[] args) throws Exception {
		new MainProgram();
	}
	
	public MainProgram() throws Exception {
		// Add escpe and enter buttons to the button listener. 
		Button.ESCAPE.addButtonListener(this);
		Button.ENTER.addButtonListener(this);
		
		// Gives the user time to remove the hands before the robot starts driving and clears the display. 
		LCD.clearDisplay();
		LCD.drawString("   Starting       ", 0, 4);
		Thread.sleep(1000);
		LCD.clearDisplay();

		communication = new PreyCommunication();
		
		// Initialize the different components to run the constructors. 
		new HeadTurner();
		new InternalState(communication);
		new Sensors();
		new SoundPlayer();
		
		// Initialize the behaviors. 
		DeadWatcher deadWatcher = new DeadWatcher("deadWatcher", 1);
//		DetectStalling detectStalling = new DetectStalling("detectStalling", 2);
		AvoidBorder avoidBorder = new AvoidBorder("avoidBorder", 3);
		AvoidObstacle avoidObst = new AvoidObstacle("avoidObst", 4);
		RunAway runAway = new RunAway("runAway", 5);
		GatherEnergy gather = new GatherEnergy("gatherEnergy", 6);
		Wander wander = new Wander("wander", 7);
		
		Behavior[] behaviors = {deadWatcher, avoidBorder, avoidObst, runAway, gather, wander};

		// Initialize and start the arbitrator. 
		new Arbitrator(behaviors, wander);

		state = GameState.READY;
		
		// Once state becomes QUITTING the entire program will exit. 
		mainLoop();

		// terminate
		quit();
	}
	
	private void mainLoop() {		
		while (state != GameState.QUITTING) {		
			PCSignal input = communication.receiveSignal();
			switch (input) {
			case START:
				startRobot();
				break;
			case STOP:
				stopRobot();
				break;
			case RESET:
				resetRobot();
				break;
			case EXIT: 
				quit();
				break;
			case EASY:
				DrivingSpeed.setDifficulty(Difficulty.EASY);
				break;
			case NORMAL:
				DrivingSpeed.setDifficulty(Difficulty.NORMAL);
				break;
			case HARD:
				DrivingSpeed.setDifficulty(Difficulty.HARD);
				break;
			default:
				break;
			}
			//LCD.drawString(state + " - " + input + "      ", 0, 7);
			try { Thread.sleep(10); } catch (InterruptedException e) { }
		}
	}

	@Override
	public void buttonPressed(Button b) {}

	@Override
	public void buttonReleased(Button b) {
		// When any button is pressed, the program terminates. 
		quit();
	}

	private void quit() {
		// Stop the arbitrator, the main program and the motors.
		state = GameState.QUITTING;
		resetMotors();
		if (communication != null) {
			communication.sendSignal(RobotSignal.TURNING_OFF);
		}
		System.exit(0);
	}
	
	private void stopRobot() {
		state = GameState.READY;
	}
	
	private void startRobot() {
		state = GameState.RUNNING;
	}
	
	private void resetRobot() {
		stopRobot();
		InternalState.reset();
		resetMotors();
	}
	
	public static GameState getState() {
		return state;
	}
	
	public static void setState(GameState state) {
		MainProgram.state = state;
	}
	
	public static PreyCommunication getCommunication() {
		return communication;
	}

	/**
	 * Stop the motors and reset the head. It does this multiple times in one 
	 * second, to make sure to overwrite all commands from delayed behaviors. 
	 */
	private void resetMotors() {
		try {
			int totalTime = 1000;
			int step = 100;
			for (int i = 0; i < totalTime; i += step) {
				Constants.LEFT_MOTOR.controlMotor(0,3);
				Constants.RIGHT_MOTOR.controlMotor(0,3);
				HeadTurner.centerHead();
				Thread.sleep(step); 
			}
		} catch (InterruptedException e) {}
	}
}
