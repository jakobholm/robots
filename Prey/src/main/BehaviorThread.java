package main;

import behaviors.Behavior;

public class BehaviorThread extends Thread {

	private volatile Behavior runningBehavior;

	public BehaviorThread() {
		this.setDaemon(true);
		this.start();
	}

	@Override
	public void run() {
		while (true) {
			try {
				if (runningBehavior != null) {
					runningBehavior.startAction();
					runningBehavior = null;
				} else {
					
					try { Thread.sleep(100); } catch (InterruptedException e) { }
				}
			} catch (InterruptBehaviorException e) { }
		}
	}

	public void startBehavior(Behavior behavior) {
		stopBehavior();
		runningBehavior = behavior;
	}

	public void stopBehavior() {
		if (runningBehavior != null) {
			runningBehavior.stopAction();
		}
		runningBehavior = null;
		interrupt();
	}
	
	public Behavior getRunningBehavior() {
		return runningBehavior;
	}
}
