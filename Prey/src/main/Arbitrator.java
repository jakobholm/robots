package main;

import helperclasses.SoundPlayer;
import lejos.nxt.LCD;
import main.Enum.GameState;
import behaviors.Behavior;

/**
 * @author HS
 * 
 */
public class Arbitrator extends Thread {
	private BehaviorThread behaviorThread;
	private Behavior defaultBehavior;
	private Behavior[] behaviors; // The list of all active behaviors.
	private Behavior runningBehavior; // The behavior from the previous run.
	private int lastMotivation = 0; // The maximum motivation from the previous run.

	public Arbitrator(Behavior[] behaviors, Behavior defaultBehavior) {
		this.behaviors = behaviors;
		this.defaultBehavior = defaultBehavior;
		this.setDaemon(true);
		behaviorThread = new BehaviorThread();
		this.start();
	}

	public void run() {
		int maxMotivation = 0; // Keeps track of the highest motivation for each run.
		int currentMotivation; // The motivation for the current behavior when running through behaviors.
		Behavior maxBehavior; // The behavior with the maximum motivation.

		while (MainProgram.getState() != GameState.QUITTING) {
			
			while (MainProgram.getState() != GameState.RUNNING) {
				LCD.drawString("Idle             ", 0, 0);
				behaviorThread.stopBehavior();
				try { Thread.sleep(100); } catch (InterruptedException e) { }
			}
			
			maxBehavior = defaultBehavior;
			maxMotivation = defaultBehavior.getMotivation();
			
			for (Behavior currentBehavior : behaviors) {
				
				// Print the behavior along with the priority and motivation to the LCD.
				LCD.drawString(currentBehavior.getBehaviorPriority() + ", "	+ currentBehavior.getMotivation() + ", " + currentBehavior.getName() + "      ", 0, currentBehavior.getBehaviorPriority());

				if (currentBehavior.equals(defaultBehavior)) { continue; }

				
				currentMotivation = currentBehavior.getMotivation();

				// If the new behavior has higher motivation or if to behaviors are tied for motivation  and the new has higher priority.
				if (currentMotivation > maxMotivation
						|| (currentMotivation == maxMotivation && maxBehavior.getBehaviorPriority() > currentBehavior.getBehaviorPriority())) {
					// A new behavior has taken the lead.
					maxBehavior = currentBehavior;
					maxMotivation = maxBehavior.getMotivation();
				}
			}

			
			// Stop runnningBehavior and starts new one, if the two aren't the same
			runningBehavior = behaviorThread.getRunningBehavior();
			if (runningBehavior == null || !maxBehavior.equals(runningBehavior)) {
//				MainProgram.getCommunication().send(RobotSignal.DEBUG, 11);
//				MainProgram.getCommunication().send(RobotSignal.DEBUG, maxBehavior.getBehaviorPriority());
				// Run the highest motivated behavior and print the name of the
				// behavior on the first line of the LCD.
				behaviorThread.startBehavior(maxBehavior);
				LCD.drawString("Running " + maxBehavior.getName() + "       ", 0, 0);

				// Prepare for the next run.
				lastMotivation = maxMotivation;

				// Lets a behavior interrupt itself if it is selfInterruptible
				// and has higher motivation
			} else if (maxBehavior.equals(runningBehavior) && maxBehavior.isSelfInterruptible() 
					&& (maxMotivation > lastMotivation || maxMotivation == Constants.MAX_MOTIVATION)) {
//				MainProgram.getCommunication().send(RobotSignal.DEBUG, 12);
//				MainProgram.getCommunication().send(RobotSignal.DEBUG, maxBehavior.getBehaviorPriority());
				// Restart behavior.
				behaviorThread.startBehavior(maxBehavior);
				
				// Prepare for the next run.
				lastMotivation = maxMotivation;
				
			} else {
			}

			// Every 10 ms the entire loop is repeated and all motivations are checked.
			try { Thread.sleep(10); } catch (InterruptedException e) { }
		}
	}
}
