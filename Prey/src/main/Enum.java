package main;


public class Enum {
	
	public enum GameState {
		RUNNING,
		READY, 
		QUITTING,
		NOT_CONNECTED;
		
		@Override 
		public String toString() {
			//only capitalize the first letter
			String s = super.toString();
			return s.substring(0, 1) + s.substring(1).toLowerCase();
		}
	}
	
	public enum BehaviorState {
		RUNNING,
//		READY, 
//		QUITTING;
		STOPPED;
		
		@Override 
		public String toString() {
			//only capitalize the first letter
			String s = super.toString();
			return s.substring(0, 1) + s.substring(1).toLowerCase();
		}
	}
	
	public enum Color {
		NONE,
//		EDGE,
		BORDER,
		ENERGY;
		
//		private final int colorID;
//
//		Color(int colorID) {
//			this.colorID = colorID;
//		}
//
//		private static final Map<Integer, Color> intToColorMap = new HashMap<Integer, Color>();
//		static {
//			for (Color c : Color.values()) {
//				intToColorMap.put(c.colorID, c);
//			}
//		}
//		
//		public static Color getColor(int i) {
//			if (i >= 0 && intToColorMap.containsKey(i)) {
//				return intToColorMap.get(i);
//			} else {
//				return NONE;
//			}
//		}
	}
	
	public enum DrivingSpeed {
		NOSPEED(0),
		SLOW(60),
		MEDIUM(70),
		FAST(80),
		FASTEST(90);
		
		private int speed;
		
		private DrivingSpeed(int speed) {
			this.speed = speed;
		}
		
		public int getSpeed() {
			return speed;
		}
		
		private void setSpeed(int speed) {
			this.speed = speed;
		}
		
		public static void setDifficulty(Difficulty difficulty) {
			switch (difficulty) {
			case EASY:
				SLOW.setSpeed(50);
				MEDIUM.setSpeed(60);
				FAST.setSpeed(70);
				FASTEST.setSpeed(80);
				break;
			case NORMAL:
				SLOW.setSpeed(55);
				MEDIUM.setSpeed(65);
				FAST.setSpeed(75);
				FASTEST.setSpeed(85);
				break;
			case HARD:
				SLOW.setSpeed(60);
				MEDIUM.setSpeed(70);
				FAST.setSpeed(80);
				FASTEST.setSpeed(90);
				break;
			default:
				break;
			}
		}
	}
	
	public enum Difficulty {
		EASY, NORMAL, HARD;

//		private static final Map<String, Difficulty> stringToDifficultyMap = new HashMap<String, Difficulty>();
//		static {
//			for (Difficulty d : Difficulty.values()) {
//				stringToDifficultyMap.put(d.toString(), d);
//			}
//		}
//		
//		public static Difficulty getDifficulty(String s) {
//			if (s != null && stringToDifficultyMap.containsKey(s)) {
//				return stringToDifficultyMap.get(s);
//			} else {
//				return null;
//			}
//		}
	}
	
	public enum DrivingMode {
		FORWARD(1),
		BACKWARD(2),
		STOP(3);
		
		private int mode;
		
		private DrivingMode(int mode) {
			this.mode = mode;
		}
		
		public int getMode() {
			return mode;
		}
		
		public DrivingMode getOpposite() {
			switch (this) {
			case BACKWARD:
				return FORWARD;
			case FORWARD:
				return BACKWARD;
			default:
				return STOP;
			}
		}
		
	}
}
