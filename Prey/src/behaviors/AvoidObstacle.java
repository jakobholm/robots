package behaviors;

import helperclasses.HeadTurner;
import helperclasses.Mover;
import helperclasses.SoundPlayer;

import java.util.Random;

import main.Constants;
import main.Enum.DrivingMode;
import main.Enum.DrivingSpeed;
import main.InterruptBehaviorException;
import main.Sensors;


public class AvoidObstacle extends Behavior {
	private Random rand = new Random();
	private final int dangerZoneThreshold = 15; // Defines the maximum distance (in cm) for this behavior to react. 
	private final int safeZoneThreshold = 35;
	private int safeZoneCounter = 0;
	private DrivingSpeed speed = DrivingSpeed.FAST;
	
	public AvoidObstacle(String name, int priority) {
		super(name, priority, true);
	}

	@Override
	public int getMotivation() {
		// Saves the measurement of the ultrasonic sensor. 
		int distance = Sensors.getUltraSonicDistance();
		safeZoneDetector(distance);
		
		if (isRunning()) {
			// If the robot is avoiding an obstacle the motivations is at least 50. 
			// This makes sure that it takes something important to take precedence. 
			return (int) Math.max(Constants.RUNNING_MOTIVATION, (dangerZoneThreshold-distance)*(Constants.MAX_MOTIVATION/dangerZoneThreshold));
		} else {
			return (int) Math.max(0, (dangerZoneThreshold-distance)*(Constants.MAX_MOTIVATION/dangerZoneThreshold));
		}		
	}

	@Override
	public void action() throws InterruptBehaviorException {
		// Stops the motors and resets the head. 
		int headDirection = HeadTurner.getCurrentPosition();
		
		// Asynchronous sound call. 
		SoundPlayer.playAvoidObstacle();
		
		// The back off depends upon the direction of the head. 
		switch (headDirection) {
			case HeadTurner.CENTER:
				// If the head was centered the robot backs up. 
				break;
			case HeadTurner.LEFT:
				// If the head was turning left, turn backwards right to line up the robot. 
				Mover.drive(speed, DrivingMode.BACKWARD, DrivingSpeed.NOSPEED, DrivingMode.BACKWARD);
				delay(250 + rand.nextInt(250));
				break;	
			case HeadTurner.RIGHT:
				// If the head was turning right, turn backwards left to line up the robot. 
				Mover.drive(DrivingSpeed.NOSPEED, DrivingMode.BACKWARD, speed, DrivingMode.BACKWARD);
				delay(250 + rand.nextInt(250));
				break;	
		}
		// Back up, and turn backwards in random direction
		Mover.drive(speed, DrivingMode.BACKWARD, speed, DrivingMode.BACKWARD);
		delay(500 + rand.nextInt(500));
		Mover.turnInRandomdirection(speed, DrivingSpeed.NOSPEED, DrivingMode.BACKWARD);
		delay(500 + rand.nextInt(500));
		
	}
	
	public void safeZoneDetector(int distance) {
		if (distance <= safeZoneThreshold) {
			safeZoneCounter++;
		} else {
			safeZoneCounter = 0;
		}	
	}
}
