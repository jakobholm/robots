package behaviors;

import helperclasses.Mover;
import helperclasses.SoundPlayer;
import main.Enum.BehaviorState;
import main.InterruptBehaviorException;

/**
 * @author HS
 * 
 */
public abstract class Behavior {

	private volatile BehaviorState state = BehaviorState.STOPPED;

	private String name; // The name of the behavior. Used to compare behaviors.
	private int priority; // The (unique) priority of the behavior is used when
	// two behaviors have the same motivation.
	private boolean selfInterruptible; // Some behaviors have to be able to
	// interrupt itself (e.g. AvoidObstacle)
	// and some should not (e.g. EatFood).

	/**
	 * @param name
	 *            the name of the behavior
	 * @param priority
	 *            the priority of the behavior
	 * @param selfInterruptible
	 *            if the behavior is self-interruptible
	 */
	public Behavior(String name, int priority, boolean selfInterruptible) {
		this.name = name;
		this.priority = priority;
		this.selfInterruptible = selfInterruptible;
	}

	/**
	 * @param name
	 *            the name of the behavior
	 * @param priority
	 *            the priority of the behavior
	 */
	public Behavior(String name, int priority) {
		this.name = name;
		this.priority = priority;
		this.selfInterruptible = false;
	}

	/**
	 * @return the name of the behavior
	 */
	public String getName() {
		return name;
	}

	public BehaviorState getState() {
		return state;
	}

	/**
	 * @return the priority of the behavior
	 */
	public int getBehaviorPriority() {
		return priority;
	}

	/**
	 * @return true if the behavior is running
	 */
	public boolean isRunning() {
		return state == BehaviorState.RUNNING;
	}

	/**
	 * @param selfInterruptible
	 *            true if the behavior is self interruptible
	 */
	public void setSelfinterruptible(boolean selfInterruptible) {
		this.selfInterruptible = selfInterruptible;
	}

	/**
	 * @return true if the behavior is self interruptible
	 */
	public boolean isSelfInterruptible() {
		return selfInterruptible;
	}

	/**
	 * Used instead of sleep. 
	 * 
	 * @param time
	 *            duration to sleep
	 * @throws InterruptBehaviorException 
	 */
	public void delay(int time) throws InterruptBehaviorException {
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {

		} finally {
			if (state == BehaviorState.STOPPED) {
				throw new InterruptBehaviorException(); 
			}
		}
	}

	/**
	 * The getMotivation must be implemented in each individual behavior.
	 * 
	 * @return the current motivation as an int from 0 to 100
	 */
	public abstract int getMotivation();

	/**
	 * The action method is the behaviors actual behavior, so for the behavior
	 * AvoidBorder the action is to avoid the border and so on. Everything in
	 * the action must be either interruptible such as delay() and calls to the
	 * Mover, or have practically no running time. 
	 * 
	 * If action is a series of maneuvers it should be ended by actionDone().
	 * 
	 * @throws InterruptBehaviorException
	 *             if the action is at some point interrupted
	 */
	protected abstract void action() throws InterruptBehaviorException;

	public void startAction() throws InterruptBehaviorException {
		try {
			state = BehaviorState.RUNNING;
			action();
		} catch (InterruptBehaviorException e) {
			throw e;
		} finally {
			SoundPlayer.stopPlaying();
			state = BehaviorState.STOPPED;
		}
	}
	
	/**
	 * Stops the action of the behavior and interrupts the Mover. Called by the
	 * Arbitrator.
	 */
	public void stopAction() {
		state = BehaviorState.STOPPED;
		try { Mover.stop(); } catch (InterruptBehaviorException e) {}
	}

	
	@Override
	public boolean equals(Object other){
		if (other == null) {
			return false;
		} else if (other == this) {
			return true;
		} else if (other instanceof Behavior) {
			return getName().equals(((Behavior) other).getName());
		} else {
			return false;
		}
	}
	
	public String toString() {
		return getName();
	}
}
