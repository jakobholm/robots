package behaviors;


import helperclasses.HeadTurner;
import helperclasses.Mover;
import helperclasses.SoundPlayer;

import java.util.Random;

import main.Enum.DrivingMode;
import main.Enum.DrivingSpeed;
import main.Constants;
import main.InterruptBehaviorException;
import main.Sensors;



public class RunAway extends Behavior {
	private Random rand = new Random();
	private Random randBool = new Random();
	private int threshold = 130;
	private DrivingSpeed fastSpeed = DrivingSpeed.FASTEST;
	
	
	public RunAway(String name, int priority) {
		super(name, priority, true);
	}

	@Override
	public int getMotivation() {
		int avr = Sensors.getAverageInfraredValues();
		
		if (avr > threshold) {		//Really close
			setSelfinterruptible(false);
			return Constants.MAX_MOTIVATION;
		} else {
			setSelfinterruptible(true);
			return (isRunning() ? Constants.RUNNING_MOTIVATION : 0);
		}
		
	}

	@Override
	public void action() throws InterruptBehaviorException {
		int headDirection = HeadTurner.getCurrentPosition();
		
		SoundPlayer.playRunAway();
		
		// Turn on the spot
		switch (headDirection) {
			case HeadTurner.CENTER:
				if (randBool.nextBoolean()) {
					Mover.drive(DrivingSpeed.FASTEST, DrivingMode.FORWARD, DrivingSpeed.FASTEST, DrivingMode.BACKWARD);
				} else {
					Mover.drive(DrivingSpeed.FASTEST, DrivingMode.BACKWARD, DrivingSpeed.FASTEST, DrivingMode.FORWARD);
				}
				break;
			case HeadTurner.LEFT:
				// If the head was turning left, turn right.
				Mover.drive(DrivingSpeed.FASTEST, DrivingMode.FORWARD, DrivingSpeed.FASTEST, DrivingMode.BACKWARD);
				break;	
			case HeadTurner.RIGHT:
				// If the head was turning right, turn left .
				Mover.drive(DrivingSpeed.FASTEST, DrivingMode.BACKWARD, DrivingSpeed.FASTEST, DrivingMode.FORWARD);
				break;	
		}
		delay(600);

		// Move away from the robot fast
		Mover.drive(DrivingSpeed.FASTEST, DrivingMode.FORWARD, DrivingSpeed.FASTEST, DrivingMode.FORWARD);
		delay(1750 + rand.nextInt(500));
		
		
		// Turn on the spot
		if (randBool.nextBoolean()) {
			Mover.drive(DrivingSpeed.MEDIUM, DrivingMode.FORWARD, DrivingSpeed.MEDIUM, DrivingMode.BACKWARD);
		} else {
			Mover.drive(DrivingSpeed.MEDIUM, DrivingMode.BACKWARD, DrivingSpeed.MEDIUM, DrivingMode.FORWARD);
		}
		
		delay(900 + rand.nextInt(250));
		Mover.stop();
		
		// Look around
		HeadTurner.turnLeft();
		delay(800);
		HeadTurner.turnRight();
		delay(800);

	}
}
