package behaviors;

import helperclasses.HeadTurner;
import helperclasses.Mover;
import helperclasses.SoundPlayer;
import main.Enum.Color;
import main.InternalState;
import main.InterruptBehaviorException;
import main.Sensors;

public class GatherEnergy extends Behavior {	
	/* The energy is encreased when gathering, and this is done on the getMotivation, seeing that it is called periodically.
	 * If the energy was incremented every time the getMotivation was called, it would go from 100 to zero in about one second.
	 * To prevent this the gatheringCounter increments every time getMotivation is called, and once it reaches maxGatheringCount 
	 * energy is decremented.
	 */
	private int gatheringCounter = 0;
	private int maxGatheringCount = 20;

	public GatherEnergy(String name, int priority) {
		super(name, priority);
	}

	@Override
	public int getMotivation() {
		
		if (isRunning()) {
			// While the robot is standing on top of energy and this behavior is the leading behavior, the robot gathers and energy is decreased. 
			gatheringCounter++;
			if (gatheringCounter == maxGatheringCount) {
				InternalState.incrementEnergy();
				gatheringCounter = 0;
			}
		}
		
		Color color = Sensors.getColourSensorValue();
		if (color == Color.ENERGY) {
			// If the robot is standing on top of food, the motivation depends upon the energy, ranging from 0 to 90. 
			return (int) (90 - InternalState.getEnergy()*0.9);
		} else {
			// If the robot can not see food, there is no point in gathering. 
			return 0;
		}
	}

	@Override
	public void action() throws InterruptBehaviorException {
		// The gathering behavior is standing still an turning the head from left to right. 
		Mover.stop();
		
		HeadTurner.turnFastLeft();
		SoundPlayer.playGatherEnergy();
		delay(500);
		
		HeadTurner.turnFastRight();
		delay(500);
	}
}
