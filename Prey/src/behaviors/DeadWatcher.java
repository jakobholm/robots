package behaviors;

import helperclasses.SoundPlayer;
import main.Enum.GameState;
import main.InterruptBehaviorException;
import main.MainProgram;
import main.Sensors;

public class DeadWatcher extends Behavior {

	
	public DeadWatcher(String name, int priority) {
		// Calls the super constructor.
		super(name, priority);
	}

	@Override
	public int getMotivation() {
		// If the touch sensor is pushed it is most important.
		if (Sensors.getTouchSensorValue()) {
			return 100; 
		} else {
			return 0;
		}
	}

	@Override
	public void action() throws InterruptBehaviorException {
		// To avoid something behind the robot, the robot drives forward for a bit.
		SoundPlayer.playDie();
	
		MainProgram.setState(GameState.READY);
		MainProgram.getCommunication().sendDeathSignal();
	}
}
