package behaviors;


import helperclasses.HeadTurner;
import helperclasses.Mover;

import java.util.Random;

import main.Enum.DrivingMode;
import main.Enum.DrivingSpeed;
import main.InterruptBehaviorException;

public class Wander extends Behavior {
	private Random rand = new Random();
	private Random boolRand = new Random();
	
	private final int lookAroundMax = 5;
	private int lookAroundCounter = 0;
	
	private DrivingSpeed maxSpeed = DrivingSpeed.FAST;
	private DrivingSpeed slowSpeed = DrivingSpeed.SLOW;
	
	public Wander(String name, int priority) {
		super(name, priority);
	}

	@Override
	public int getMotivation() {
		return 1;
	}

	@Override
	public void action() throws InterruptBehaviorException {
		
//		Mover.drive(DrivingSpeed.FAST, DrivingMode.BACKWARD, DrivingSpeed.FAST, DrivingMode.BACKWARD);
//		delay(10000);
		
		
		
		
		
		
		
		
		// Slow down, and look around
		if (lookAroundCounter > lookAroundMax) {
			Mover.drive(DrivingSpeed.SLOW, DrivingMode.FORWARD, DrivingSpeed.SLOW, DrivingMode.FORWARD);
			delay(500);
			Mover.stop();
			HeadTurner.turnLeft();
			delay(1000);
			HeadTurner.turnFastRight();
			delay(1000);
			HeadTurner.turnFastLeft();
			delay(1000);
			HeadTurner.centerHead();
			delay(250);
			lookAroundCounter = 0;
			return;
		}
		lookAroundCounter++;
//		MainProgram.getCommunication().send(RobotSignal.DEBUG, lookAroundCounter);
		
		
		switch (rand.nextInt(3)) {
		case 0:
			slowSpeed = DrivingSpeed.SLOW;
			break;
		case 1:
			slowSpeed = DrivingSpeed.MEDIUM;
			break;
		case 2:
			slowSpeed = DrivingSpeed.FAST;
			break;		
		}
		
		if (boolRand.nextBoolean()) {
			// possible turn right
			Mover.drive(maxSpeed, DrivingMode.FORWARD, slowSpeed, DrivingMode.FORWARD);
		} else {
			// possible turn left
			Mover.drive(slowSpeed, DrivingMode.FORWARD, maxSpeed, DrivingMode.FORWARD);
		}
		delay(1000 + rand.nextInt(1000));
	}
}
