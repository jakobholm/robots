package behaviors;

import helperclasses.Mover;
import helperclasses.Signal.RobotSignal;
import helperclasses.SoundPlayer;

import java.util.Random;

import main.Enum.DrivingMode;
import main.Enum.DrivingSpeed;
import main.InterruptBehaviorException;
import main.MainProgram;

public class DetectStalling extends Behavior {
	private Random rand = new Random();
	private DrivingSpeed speed = DrivingSpeed.MEDIUM;
	
	
	public DetectStalling(String name, int priority) {
		super(name, priority);	// Not self interruptable
	}

	
	@Override
	public int getMotivation() {		
//		Mover.isLeftMotorStalling();
		
		if (isRunning()) {
			return 50;
		} else if (Mover.isRightMotorStalling()) {
			MainProgram.getCommunication().send(RobotSignal.DEBUG, 0);
			return 100;
//			return 0;
		} else {
			return 0;
		}
	}

	
	@Override
	protected void action() throws InterruptBehaviorException {
		if (Mover.getRobotDrivingMode() == DrivingMode.BACKWARD) {
			Mover.drive(speed, DrivingMode.FORWARD, speed, DrivingMode.FORWARD);
//			delay(250 + rand.nextInt(250));
			delay(5000 + rand.nextInt(250));		//Test
			Mover.turnInRandomdirection(speed, DrivingSpeed.NOSPEED, DrivingMode.BACKWARD);
		} else {
			Mover.drive(speed, DrivingMode.BACKWARD, speed, DrivingMode.BACKWARD);
//			delay(250 + rand.nextInt(250));
			delay(5000 + rand.nextInt(250));
			Mover.turnInRandomdirection(speed, DrivingSpeed.NOSPEED, DrivingMode.FORWARD);
		}
	}
	
	
	
	
	
	
	
	
	
	

}
