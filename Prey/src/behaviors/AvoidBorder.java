package behaviors;


import helperclasses.Mover;
import helperclasses.SoundPlayer;

import java.util.Random;

import main.Constants;
import main.Enum.Color;
import main.Enum.DrivingMode;
import main.Enum.DrivingSpeed;
import main.InterruptBehaviorException;
import main.Sensors;

public class AvoidBorder extends Behavior {
	private Random rand = new Random();
	private DrivingSpeed turningSpeed = DrivingSpeed.MEDIUM;
	private DrivingSpeed drivingSpeed = DrivingSpeed.MEDIUM;
	private DrivingMode turningMode = DrivingMode.FORWARD;
	private int forwardTachoDegrees = 35;
	private int tachoTurningLimit = 880;
	private Color condColor = Color.BORDER;


	public AvoidBorder(String name, int priority) {
		// Calls the super constructor. NOTE: selfInterruptable=true. 
		super(name, priority);
	}

	@Override
	public int getMotivation() {
		// Gets and saves the color id from the Sensors class. 
		Color color = Sensors.getColourSensorValue();

		if (color == Color.BORDER) {
			// If the robot is on the border this behavior is more important than anything else. 
			return Constants.MAX_MOTIVATION;
		} else {
			// If the robot is not on the border it should be self-interruptible. 
//			setSelfinterruptible(true);
			// If the robot is not on the border it returns 50 if its running (i.e. avoiding the border) or 0 otherwise. 
			return (isRunning() ? Constants.RUNNING_MOTIVATION : 0);
		}

	}

	@Override
	public void action() throws InterruptBehaviorException {
		/* Make sure the behavior does not interrupt itself while on the border. 
		 * Otherwise the robot would newer get to the move forward part before it is interrupted by itself. */
		SoundPlayer.playAvoidBorder();
		
		int leftTachoInit = Mover.getLeftMotorTachoCount();
		int rightTachoInit = Mover.getRightMotorTachoCount();
		int leftTachoCurrent = 0, rightTachoCurrent = 0;

		// Drive into/out of the border
		Mover.drive(drivingSpeed, DrivingMode.FORWARD, drivingSpeed, DrivingMode.FORWARD);
		while(leftTachoCurrent < forwardTachoDegrees && rightTachoCurrent < forwardTachoDegrees) {
			leftTachoCurrent = Mover.getLeftMotorTachoCount() - leftTachoInit;
			rightTachoCurrent = Mover.getRightMotorTachoCount() - rightTachoInit;
		}

		// Turn in random direction
		if (rand.nextBoolean()) {
			turningMode = turningMode.getOpposite();
		}

		// If moving foward
		if (Mover.getRobotDrivingMode() == DrivingMode.FORWARD) {
			tachoTurningLimit = 820;
			condColor = Color.BORDER;
		} else {
			tachoTurningLimit = 500;
			condColor = Color.NONE;
		}

		// Turn on the spot
		leftTachoInit = Mover.getLeftMotorTachoCount();
		rightTachoInit = Mover.getRightMotorTachoCount();
		
		Mover.drive(turningSpeed, turningMode, turningSpeed, turningMode.getOpposite());
		while (Sensors.getColourSensorValue() == condColor
				&& Math.max(Mover.getLeftMotorTachoCount() - leftTachoInit, Mover.getRightMotorTachoCount() - rightTachoInit) < tachoTurningLimit) {
			delay(10);
		}

		// Turn on the spot in the same direction as above, and a bit forward
		Mover.drive(turningSpeed, turningMode, turningSpeed, turningMode.getOpposite());
		delay(400 + rand.nextInt(100));
		Mover.drive(drivingSpeed, DrivingMode.FORWARD, drivingSpeed, DrivingMode.FORWARD);
		delay(600 + rand.nextInt(300));
	}
}
