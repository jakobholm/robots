package helperclasses;


import helperclasses.Signal.RobotSignal;

import java.util.Random;

import lejos.nxt.LCD;
import lejos.nxt.MotorPort;

import main.Constants;
import main.Enum.DrivingMode;
import main.Enum.DrivingSpeed;
import main.InterruptBehaviorException;
import main.MainProgram;



/**
 * Basecode is copied from Ole Caprani's Car.java
 * 
 */
public class Mover {

	private static MotorPort leftMotor = Constants.LEFT_MOTOR;
	private static MotorPort rightMotor = Constants.RIGHT_MOTOR;
	private static Random rand = new Random();

	private static boolean interrupt = false;
	
	private static DrivingSpeed currenLeftMotorSpeed = DrivingSpeed.NOSPEED;
	private static DrivingSpeed currenRightMotorSpeed = DrivingSpeed.NOSPEED;
	
	// Stalling variables
	private static int leftTachoThreshold = 0, rightTachoThreshold = 0;
	private static int leftSlowCounter = 0, rightSlowCounter = 0;
	private static int lastLeftTacho = 0;
	private static int lastRightTacho = 0;
	private static int leftStallingTimeCounter = 0, rightStallingTimeCounter = 0;
	private static final int stallingTimeCounterMax = 15;
	private static DrivingMode lastStallingDirection = null;
	private static int leftTachoCurrent, rightTachoCurrent, leftTachoIncrease, rightTachoIncrease;
	private static final int slowCounterThreshold = 4;
	private static final int stallingThresholdCorrection = 60;
	private static long driveStartTime = 0;
	
	//TEST
	private static final int numOfSamples = 10;
	private static boolean rightSamplingDone = false;
	private static int rightSampleCounter = 0;
	private static int rightSampleSpeed = 0;
	private static int regularRightTachoIncrease_FastSpeed = 0;
	
	
	
	
	
	private static DrivingMode currentDrivingMode = DrivingMode.STOP;

	private Mover() {} 

	public static void stop() throws InterruptBehaviorException {
		drive(DrivingSpeed.NOSPEED, DrivingMode.STOP, DrivingSpeed.NOSPEED, DrivingMode.STOP);
		resetTacho();
	}

	
	public static void drive(DrivingSpeed leftMotorSpeed, DrivingMode leftMotorMode, DrivingSpeed rightMotorSpeed, DrivingMode rightMotorMode) 
		throws InterruptBehaviorException {
		
		if (interrupt)  { 
			interrupt = false;
			throw new InterruptBehaviorException();
		}
		
		driveStartTime = System.currentTimeMillis();
		
		currenLeftMotorSpeed = leftMotorSpeed;
		currenRightMotorSpeed = rightMotorSpeed;
		

		// Set local driving mode
		if (rightMotorMode == leftMotorMode) {
			currentDrivingMode = leftMotorMode;
		} else if (leftMotorSpeed == rightMotorSpeed) {
			// Turning on the spot
			currentDrivingMode = DrivingMode.FORWARD;
		} else if (leftMotorMode == DrivingMode.STOP) {
			currentDrivingMode = rightMotorMode;
		} else  if (rightMotorMode == DrivingMode.STOP) {
			currentDrivingMode = leftMotorMode;
		} else if (leftMotorSpeed.getSpeed() > rightMotorSpeed.getSpeed()) {
			currentDrivingMode = leftMotorMode;
		} else if (rightMotorSpeed.getSpeed() > leftMotorSpeed.getSpeed()) {
			currentDrivingMode = rightMotorMode;
		} else {
			currentDrivingMode = DrivingMode.STOP;
		}
		
		// Turn head if able
		turnHead(leftMotorSpeed, rightMotorSpeed);
		
		leftMotor.controlMotor(leftMotorSpeed.getSpeed(), leftMotorMode.getMode());
		rightMotor.controlMotor(rightMotorSpeed.getSpeed(), rightMotorMode.getMode());		
	}
	
	
	private static void turnHead(DrivingSpeed leftSpeed, DrivingSpeed rightSpeed) {
		if (currentDrivingMode == DrivingMode.FORWARD) {
			
			int diff = leftSpeed.getSpeed() - rightSpeed.getSpeed();
			if (Math.abs(diff) <= 5) {
				HeadTurner.centerHead();
			} else if (diff < 0) {
				HeadTurner.turnLeft();
			} else {
				HeadTurner.turnRight();
			}
				
		}
	}
	

	public static void interruptBehavior()  {
		interrupt = true;
	}

	
	public static void interrupted() throws InterruptBehaviorException {
		if (interrupt) {
			throw new InterruptBehaviorException();
		}
	}

	
	public static boolean turnInRandomdirection(DrivingSpeed highSpeed, DrivingSpeed lowSpeed, DrivingMode drivingMode) throws InterruptBehaviorException {
		if (interrupt)  { 
			interrupt = false;
			throw new InterruptBehaviorException();
		}

		currentDrivingMode = drivingMode;

		boolean isTurningRight = rand.nextBoolean();
		if (isTurningRight) {
			drive(highSpeed, drivingMode, lowSpeed, drivingMode);
//			leftMotor.controlMotor(highSpeed.getSpeed(), drivingMode.getMode());
//			rightMotor.controlMotor(lowSpeed.getSpeed(), drivingMode.getMode());
			HeadTurner.turnRight();
		} else {
			drive(lowSpeed, drivingMode, highSpeed, drivingMode);
//			rightMotor.controlMotor(highSpeed.getSpeed(), drivingMode.getMode());
//			leftMotor.controlMotor(lowSpeed.getSpeed(), drivingMode.getMode());
			HeadTurner.turnLeft();
		}
		
		return isTurningRight;
	}

	
	public static DrivingMode getRobotDrivingMode() {
		return currentDrivingMode;
	}
	
	
	
	public static int getLeftMotorTachoCount() {
		return leftMotor.getTachoCount();
	}
	
	
	public static int getRightMotorTachoCount() {
		return rightMotor.getTachoCount();
	}
	
	
	private static void resetTacho() {
		leftMotor.resetTachoCount();
		rightMotor.resetTachoCount();
	}
	
	
	public static boolean isMoving() {
		if (currentDrivingMode == DrivingMode.BACKWARD || currentDrivingMode == DrivingMode.FORWARD) {
			return true;
		} 
		
		return false;
	}
	
	
	public static boolean isLeftMotorStalling() {
		leftStallingTimeCounter++;
		if (!isMoving()) {
			return false;
		}
		
		// If 100 ms hasn't passed after started driving don't detect stalling
		if (System.currentTimeMillis() - driveStartTime <= 500) {
			return false;
		}
		
		
		leftTachoCurrent = getLeftMotorTachoCount();
		leftTachoIncrease = Math.max(leftTachoCurrent - lastLeftTacho, 0);
		

		if (leftStallingTimeCounter >= stallingTimeCounterMax) {
			leftStallingTimeCounter = 0;
			
			// Set tacho threshold with black magic
			switch (currenLeftMotorSpeed) {
				case SLOW:
					leftTachoThreshold = 5;
					break;
					
				case MEDIUM:
					leftTachoThreshold = 35;
					break;
					
				case FAST:
					leftTachoThreshold = 75;
					break;
					
				case FASTEST:
					leftTachoThreshold = 90;
					break;
			}
			
			
			
			//TEST
			MainProgram.getCommunication().send(RobotSignal.DEBUG, leftTachoIncrease);
			LCD.drawString("LIncrease: " + leftTachoIncrease + "        ", 0, 3);
			LCD.drawString("Lslowcount: " + leftSlowCounter + "  ", 0, 4);
			
			lastLeftTacho = leftTachoCurrent;

			
			if (leftTachoIncrease <= leftTachoThreshold && currentDrivingMode == lastStallingDirection) {
				leftSlowCounter++;
			} else {
				leftSlowCounter = 0;
			}
			
			if (leftSlowCounter >= slowCounterThreshold) {
				leftSlowCounter = 0;	
				return true;
			}
			
			
			lastStallingDirection = currentDrivingMode;
		}
		
		return false;
	}
	
	public static boolean isRightMotorStalling() {
		rightStallingTimeCounter++;
		if (!isMoving()) {
			return false;
		}
		
		// If 100 ms hasn't passed after started driving don't detect stalling
		if (System.currentTimeMillis() - driveStartTime <= 500) {
			return false;
		}
		
		
		
		if (rightStallingTimeCounter >= stallingTimeCounterMax) {
			rightStallingTimeCounter = 0;
			
			rightTachoCurrent = getRightMotorTachoCount();
			rightTachoIncrease = Math.max(rightTachoCurrent - lastRightTacho, 0);
			lastRightTacho = rightTachoCurrent;
			
			
			// TEST SAMPLING
			// If sampling of speed to dertemine regular speed isn't done 
			// perform sampling and return false
			if (rightSampleCounter < numOfSamples-1 && !rightSamplingDone) {
				// Sampling
				rightSampleSpeed += rightTachoIncrease;
				rightSampleCounter++;
				return false;
			} else if (!rightSamplingDone) {
				
				int regSpeed = rightSampleSpeed/numOfSamples;
				
				// Should depend on current speed
				regularRightTachoIncrease_FastSpeed = regSpeed;
				
				
				//Should depend on the different speeds
				rightTachoThreshold = regularRightTachoIncrease_FastSpeed*3/4 +5;
				
				rightSampleCounter = 0;
				rightSampleSpeed = 0;
				rightSamplingDone = true;
			}
			
			
			
			// Set tacho threshold with black magic
			
			//TODO: testet med fast og fastest
//			switch (currenRightMotorSpeed) {
//				case SLOW:
//					rightTachoThreshold = 5;
//					break;
//					
//				case MEDIUM:
//					rightTachoThreshold = 35;
//					break;
//					
//				case FAST:
//					rightTachoThreshold = 65;
//					break;
//					
//				case FASTEST:
//					rightTachoThreshold = 90;
//					break;
//			}
			
			
			//TEST
			if (rightTachoIncrease > 0) { 

				MainProgram.getCommunication().send(RobotSignal.DEBUG, rightTachoIncrease);
				//MainProgram.getCommunication().send(RobotSignal.DEBUG, currenRightMotorSpeed.getSpeed());
			}
			LCD.drawString("RIncrease: " + rightTachoIncrease + "        ", 0, 3);
			LCD.drawString("Rslowcount: " + rightSlowCounter + "  ", 0, 4);
			LCD.drawString("regSpeed " + regularRightTachoIncrease_FastSpeed + "   " , 0, 5);
			LCD.drawString("rTT " + rightTachoThreshold + "   " , 0, 6);
			
			
			if (rightTachoIncrease <= rightTachoThreshold && currentDrivingMode == lastStallingDirection) {
				rightSlowCounter++;
			} else {
				rightSlowCounter = 0;
			}
			
			if (rightSlowCounter >= slowCounterThreshold) {
				rightSlowCounter = 0;	
				return true;
			}
			
			
			lastStallingDirection = currentDrivingMode;
		}
		
		return false;
	}
	
	
//	public static boolean isStalling() {
//		stallingTimeCounter++;
//		if (!isMoving()) {
//			return false;
//		}
//		
//		if (stallingTimeCounter >= stallingTimeCounterMax) {
//			stallingTimeCounter = 0;
//			
//			leftTachoCurrent = getLeftMotorTachoCount();
//			rightTachoCurrent = getRightMotorTachoCount();
//			leftTachoIncrease = Math.max(leftTachoCurrent - lastLeftTacho, 0);
//			rightTachoIncrease = Math.max(rightTachoCurrent - lastRightTacho, 0);
//			
//
//			LCD.drawString("LIncrease: " + leftTachoIncrease + "        ", 0, 3);
//			LCD.drawString("RIncrease: " + rightTachoIncrease + "        ", 0, 4);
//			LCD.drawString("slowcount: " + slowCounter + "  ", 0, 5);
//			
//			
//			lastLeftTacho = leftTachoCurrent;
//			lastRightTacho = rightTachoCurrent;
//			
//			
//			// If either wheel are stuck
//			if ((leftTachoIncrease <= tachoTreshold || rightTachoIncrease <= tachoTreshold) 
//					&& currentDrivingMode == lastStallingDirection) {
//				slowCounter++;
//			} else {
//				slowCounter = 0;
//			}
//			
//			if (slowCounter >= slowCounterThreshold) {
//				slowCounter = 0;	
//				return true;
//			}
//			
//			
//			lastStallingDirection = currentDrivingMode;
//		}
//		
//		return false;
//	}
	
}
