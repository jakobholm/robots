package helperclasses;

import lejos.nxt.NXTRegulatedMotor;
import main.Constants;

public class HeadTurner {
	private final static int turningSpeed = 150; // Degrees per second
	private final static int fastTurningSpeed = 300; // Degrees per second
	private final static int fastAccelleration = 500; // Degrees per second
	private final static int accelleration = 150; // Degrees per second
	private final static int turningDegrees = 50; // Degrees per second
	public final static int CENTER = 0, LEFT = 1, RIGHT = 2; // Used by AvoidObstacle.  
	private final static int margin = 15; // If the angle of the head is between +/- margin the head is centered. 
	private static int lastDirection = 0; // The direction of the last command. 
	private static NXTRegulatedMotor regMotor = new NXTRegulatedMotor(Constants.HEAD_MOTOR);
	
	/**
	 * The constructor of the HeadTurner. 
	 * Should always be called, to set the appropriate speed and direction. 
	 */
	public HeadTurner() {
		regMotor.setSpeed(turningSpeed);
		regMotor.setAcceleration(accelleration);
	}
	
	/**
	 * Turn the head left in a moderate speed. 
	 */
	public static void turnLeft() {
		// Sets the desired turning speed and acceleration. 
		regMotor.setSpeed(turningSpeed);
		regMotor.setAcceleration(accelleration);
		
		/* If the last head movement command was to the left - the head being 
		 * at the left or on the way to the left - the command will not be 
		 * repeated.
		 */ 
		if ((regMotor.isMoving() && lastDirection != LEFT) || !regMotor.isMoving()) {
			regMotor.rotateTo(-turningDegrees, true);
		}
		lastDirection = LEFT;
	}
	
	/**
	 * Turn the head right in a moderate speed. 
	 */
	public static void turnRight() {
		regMotor.setSpeed(turningSpeed);
		regMotor.setAcceleration(accelleration);
		if ((regMotor.isMoving() && lastDirection != RIGHT) || !regMotor.isMoving()) {
			regMotor.rotateTo(turningDegrees, true);
		}
		lastDirection = RIGHT;
	}
	
	/**
	 * Center the head in a moderate speed. 
	 */
	public static void centerHead() {
		regMotor.setSpeed(turningSpeed);
		regMotor.setAcceleration(accelleration);
		if ((regMotor.isMoving() && lastDirection != CENTER) || !regMotor.isMoving()) {
			regMotor.rotateTo(0, true);
		}
		lastDirection = CENTER;
	}
	
	/**
	 * Turn the head left in a fast movement. 
	 */
	public static void turnFastLeft() {
		regMotor.setSpeed(fastTurningSpeed);
		regMotor.setAcceleration(fastAccelleration);
		
		if ((regMotor.isMoving() && lastDirection != LEFT) || !regMotor.isMoving()) {
			regMotor.rotateTo(-turningDegrees, true);
		}
		lastDirection = LEFT;
	}
	
	/**
	 * Turn the head right in a fast movement. 
	 */
	public static void turnFastRight() {
		regMotor.setSpeed(fastTurningSpeed);
		regMotor.setAcceleration(fastAccelleration);
		
		if ((regMotor.isMoving() && lastDirection != RIGHT) || !regMotor.isMoving()) {
			regMotor.rotateTo(turningDegrees, true);
		}
		lastDirection = RIGHT;
	}
	
	/**
	 * Returns the position of the head. 
	 * @return the current position of the head
	 */
	public static int getCurrentPosition() {
		int pos = regMotor.getPosition();
		if (pos <= -margin) {
			return LEFT;
		} else if (pos >= margin) {
			return RIGHT;
		} else {
			return CENTER;
		}
	}
}
