package helperclasses;

import java.io.File;
import java.util.Random;
import lejos.nxt.*;
 
public class SoundPlayer 
{

    private enum SoundEvent {
    	AvoidObstacle, AvoidBorder, GatherEnergy, 
    	Die, RunAway, NoSound
    }
    private static Player player;
    private static SoundEvent soundEvent;

    static {
    	soundEvent = SoundEvent.NoSound;
    	player = new Player();
    	player.setDaemon(true);
    	player.start();
    }
 
    private static class Player extends Thread
    {
        private static int volume = 80;
        private static int base = 32;
        private static int [] scale = new int[6];
        
        private static File boingSound = new File("boing28kHz.wav");
 	    
        private static Random rand = new Random();
        
        static
        {
			int high = 40*base;
    	    scale[0] = high;
    	    scale[1] = high*9/8;
    	    scale[2] = high*5/4;
    	    scale[3] = high*3/2;
    	    scale[4] = high*5/3;
    	    scale[5] = high*2;
        }
        
        private static void playSound(int frequency, int duration, int timeout, int rangeMin, int rangeMax) 
        throws InterruptedException
        {
            if (frequency > 0) {
                Sound.playTone(frequency, duration, volume);
            } else {
                frequency = rangeMin + rand.nextInt(rangeMax-rangeMin);
                Sound.playTone(frequency, duration, volume);
            }    
            Thread.sleep(timeout);
        }
     
        private static void playSound(int frequency, int duration, int timeout) 
        throws InterruptedException
        {
            playSound(frequency, duration, timeout, 0, 0);
        }
     
        private static void playSound(int rangeMin, int rangeMax, int duration, int timeout) 
        throws InterruptedException
        {
            playSound(0, duration, timeout, rangeMin, rangeMax);
        }
            
        private static void playAvoidBorder()
        throws InterruptedException		
        {
			int note = (int)(Math.random()*6);
            playSound(scale[note]/2, 20, 30);
            playSound(scale[(note+4)%scale.length], 20, 60);
            playSound(scale[(note+1)%scale.length], 10, 20);
            playSound(scale[note]/4, 40, 40);
        }
        
    	private static void playAvoidObstacle()
    	throws InterruptedException
        {
    		int times = (int)(Math.random()*10)+6;
    	    for (int i=0; i<times; i++)
    	    {
    	    	int note = (int)(Math.random()*scale.length);



    	    	Sound.playTone(scale[note], 10);
    		    Thread.sleep(30+(int)(Math.random()*50));
    	    } 
        }
     

        private static void playDie()
        throws InterruptedException
        { 
    	    Sound.playSample(boingSound, 100);
    	    Thread.sleep(700); 		
            Sound.playTone(16, 700, volume);
     		Thread.sleep(700);
        } 
        
        private static void playGatherEnergy()
        throws InterruptedException
        {
            int step = 50; int d;
            while ( true ) {
            	
                d = 100 + (int)(Math.random()*1000);
                for (int i = 0; i < d; i += step) {
                    //int soundFrequency = (100-InternalState.getHunger())*4 + 64;
                    int soundFrequency = i/2+base;
                    playSound(soundFrequency, 2*soundFrequency, 20, step);
                }
                Thread.sleep(100 + (int)(Math.random()*1000));
            }
        }
        
        private static void playRunAway() 
        throws InterruptedException
        {
            int step = 20; int d; 
            while ( true ) {
            	
            	d = 100 + (int)(Math.random()*200);
                for (int i = 0; i < d; i += step) {
                    playSound(32*base+8*base, 32*base+16*base, 10, step);
                }
                Thread.sleep(100 + (int)(Math.random()*1000));
            }

        }
         	
    	public void run() 
    	{
    	    while (true) {
    	    	Thread.interrupted();
    	    	if ( soundEvent != SoundEvent.NoSound )
    	    	{
    	    		try {
    	    	        
    	    	        switch ( soundEvent ){
    	    	
	    	            case AvoidBorder:
    	    	    	    playAvoidBorder();
    	    	    	    soundEvent = SoundEvent.NoSound;
	    	    	        break;
    	    	        case AvoidObstacle:
    	    	    	    playAvoidObstacle();
    	    	    	    soundEvent = SoundEvent.NoSound;
    	    	    	    break;
    	    	        case GatherEnergy:
    	    	        	playGatherEnergy();
    	    	    	    soundEvent = SoundEvent.NoSound;
    	    	    	    break;
    	    	        case Die:
    	    	    	    playDie();
    	    	    	    soundEvent = SoundEvent.NoSound;
    	    	    	    break;
    	    	        case RunAway:
    	    	        	playRunAway();
    	    	    	    soundEvent = SoundEvent.NoSound;
    	    	    	    break;
						default:
							break;
    	    	        }
    	    		}
    	    	    catch (  InterruptedException  e)
    	    	    {
    	    		    Sound.playTone(100, 0, 0);
    	    		    soundEvent = SoundEvent.NoSound;
    	    	    }
    	    	}
    	    	
    	    	try {
					Thread.sleep(10);
				} catch (InterruptedException e) {}
    	    } 	        
    	}
    }

    private static void interruptPlayer() 
    {
    	if ( soundEvent != SoundEvent.NoSound )
    	{
    	    while (soundEvent == SoundEvent.Die);
    	    player.interrupt();
    	    while ( soundEvent != SoundEvent.NoSound );
    	}
    }
    
    public static void playAvoidBorder() 
    {
    	interruptPlayer();
        soundEvent = SoundEvent.AvoidBorder; 
    }

    public static void playAvoidObstacle()
    {
    	interruptPlayer();
        soundEvent = SoundEvent.AvoidObstacle;
    }
    
    public static void playDie() 
    { 
    	interruptPlayer();
    	soundEvent = SoundEvent.Die;
    }
    
    public static void playGatherEnergy()
    {
    	interruptPlayer();
        soundEvent = SoundEvent.GatherEnergy;
    }
    
    public static void playRunAway() 
    {
    	interruptPlayer();
        soundEvent = SoundEvent.RunAway;
    } 
    
    public static void stopPlaying() 
    {
    	interruptPlayer();
    	soundEvent = SoundEvent.NoSound;
    }    
} 
  