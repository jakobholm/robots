package helperclasses;

import helperclasses.Signal.RobotSignal;
import main.InternalState;

public class PreyCommunication extends Communication {

	public void send(RobotSignal signal, int h) {
		int i = signal.getValue();
		send (i, h);
	}
	
	private void send (int i, int h) {
		byte[] b = new byte[2];
		if (h < 0) {
			h = 127;
		}
		b[0] = (byte) i;
		b[1] = (byte) h;
		send(b);
	}

	@Override
	public void sendSignal(RobotSignal signal) {
		send(signal.getValue(), -1);
	}

	public void sendEnergySignal(int energy) {
		send(RobotSignal.ENERGY, energy);
	}

	public void sendDeathSignal() {
		send(RobotSignal.DEATH, InternalState.getEnergy());
	}
}
