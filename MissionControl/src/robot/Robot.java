package robot;

import main.Constants;
import main.Enum.Difficulty;
import main.Enum.RobotInfo;
import main.Enum.RobotState;
import main.GUI;
import main.Signal.PCSignal;
import main.ThreadException;

public abstract class Robot {

	protected RobotInfo robot;
	protected GUI gui;
	protected CommunicationThread communication;
	protected RobotState state = RobotState.IDLE;


	public Robot(RobotInfo robot, GUI gui) throws ThreadException {
		this.robot = robot;
		this.communication = new CommunicationThread(this);
		this.gui = gui;
	}
	public RobotState getStatus() {
		return state;
	}

	public String getAddress() {
		return robot.getAddress();
	}
	
	public boolean isConnected() {
		return communication.isConnected();
	}

	public void startRobot() {
		changeStatus(RobotState.RUNNING);
		if (isConnected()) { communication.startRobot(); }
	}

	public void stopRobot() {
		changeStatus(RobotState.IDLE);	
		if (isConnected()) { communication.stopRobot(); }
	}
	
	public void resetRobot() {
		if (isConnected()) { communication.resetRobot(); }
	}
	
	public void connectedButNotHandshaken() {
		gui.setRobotStatusIcon(robot, Constants.NOT_HANDSHAKEN_ICON);
	}

	public void connected() {
		// updating the state, both local and on the robot
		try { Thread.sleep(500); } catch (InterruptedException e) {}
		changeStatus(state);
		gui.setRobotStatusIcon(robot, "");
		switch (state) {
		case RUNNING:
			communication.startRobot();
			break;
		case DEAD:
			// probably not going to be possible
			// unless death time is extended
			break;
		case IDLE:
			// the default state, so don't send anything
			break;
		default:
			break;
		}
	}

	public void notConnected() {
		gui.setRobotStatusIcon(robot, Constants.NOT_CONNECTED_ICON);
	}

	protected void changeStatus(RobotState status) {
		this.state = status;
	}

	public RobotInfo getRobotInfo() {
		return robot;
	}

	public void endProgram() {
		if (isConnected()) {
			communication.endProgram();
		}
	}

	public String toString() {
		return robot.toString();
	}

	public void setDifficulty(Difficulty difficulty) {
		PCSignal signal = PCSignal.valueOf(difficulty.name());
		if (isConnected()) {
			communication.send(signal);
		}
	}
}
