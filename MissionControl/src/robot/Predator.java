package robot;

import main.Enum.RobotInfo;
import main.GUI;
import main.Signal.PCSignal;
import main.ThreadException;

public class Predator extends Robot {

	public Predator(RobotInfo robot, GUI gui) throws ThreadException {
		super(robot, gui);
	}

	public void executePredatorCommand(PCSignal s) {
		if (isConnected()) { 
			communication.send(s); 
		}
	}

	public void predatorHit() {
//		gui.setIcon(RobotInfo.PREDATOR, Constants.PREDATOR_HIT_ICON);
	}

}
