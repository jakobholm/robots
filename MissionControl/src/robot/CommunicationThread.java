package robot;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import lejos.nxt.remote.NXTCommand;
import lejos.pc.comm.NXTCommFactory;
import lejos.pc.comm.NXTConnector;
import main.Enum.RobotInfo;
import main.Signal.PCSignal;
import main.Signal.RobotSignal;
import main.ThreadException;


public class CommunicationThread extends Thread {

	private Robot robot; 
	private NXTConnector connector = new NXTConnector();
	private boolean connected = false;
	private boolean handshaken = false;
	private OutputStream dataOut;
	private InputStream dataIn;
	private boolean debug = false; // true if this class is allowed to syso

	/**
	 * Start the communication thread of a robot.
	 * The thread is a daemon thread.
	 * The thread is started with this method.
	 * @param robot the robot to report back to.
	 * @throws ThreadException
	 */
	public CommunicationThread(Robot robot) throws ThreadException {
		this.robot = robot;
		this.setDaemon(true);
		this.start();
	}

	@Override
	public void run() {
		connect();
		mainLoop();
	}

	private void mainLoop() {
		DataDTO input;
		while (true) {
			input = receive();

			switch (input.getSignal()) {
			case TURNING_OFF: 
				reconnect();
				continue;
			case NODATA: 
				break;
			case DEBUG: 
				System.out.println(input.getData());
				break;	
			case ENERGY:
				if (input.getData() > 0 && input.getData() <= 100) {
					((Prey) robot).setEnergy(input.getData());
				}
				break;
			case DEATH:
				((Prey) robot).dead(input.getData());
				break;
			case HIT:
				((Predator) robot).predatorHit();
			default:
				break;
			}
		}
	}

	/**
	 * Connects to the NXT via bluetooth and performs the handshake.
	 * @return true when connection is established and handshake is correct
	 */
	private boolean connect() {
		int tries = 1;
		while (true) {
			// connect and handshake
			debug("> connect: " + robot + " " + robot.getAddress());
			connected = establishConnection();
			if (connected) {
				robot.connectedButNotHandshaken();
				handshaken = handshake();
			}

			// print status
			debug("    " + tries + ") " + robot + ": connect: " + connected + " - handshake: " + handshaken);
			tries++;

			if (isConnected()) { // public boolean isConnected() { return connected && handshaken; }
				robot.connected();
				debug("    # Connected: " + robot);
				return true; 
			} else {
				if (connected) {
					runProgramOnNXT();
				}

				// reset connection
				try { connector.close(); } catch (IOException e) { }

				connected = handshaken = false;
			}
		}
	}

	private void runProgramOnNXT() {
		debug("        > runProgramOnNXT");
		try {
			String fileName = (robot.getRobotInfo() == RobotInfo.PREDATOR ? "PredatorMain.nxj" : "MainProgram.nxj");
			NXTCommand nxtCommand = new NXTCommand(connector.getNXTComm());
			nxtCommand.startProgram(fileName);
		} catch (IOException e) { e.printStackTrace(); }			
	}

	/**
	 * Connects to the NXT via bluetooth.
	 * @return true if the connection is established
	 */
	private boolean establishConnection() {
		debug("    > establishConnection " + robot);
		return connector.connectTo(robot.toString(), robot.getAddress(), NXTCommFactory.BLUETOOTH);
	}

	/**
	 * Performs the handshake with the conneced NXT.
	 * @return if the handshake is correct
	 */
	private boolean handshake() {
		debug("    > handshake " + robot);
		dataOut = connector.getOutputStream();
		dataIn = connector.getInputStream();
		boolean tempHandshaken = true;

		// send hello
		byte[] hello = new byte[] { 'R', 'C', 'P' };
		send(hello);

		// receive hello
		byte[] reply = new byte[3];
		receive(reply);
		debug("    hello: [" + hello[0] + "," + hello[1] + "," + hello[2] + "] - reply: [" + reply[0] + "," + reply[1] + "," + reply[2] + "]");

		// check that reply matches hello
		for (int i = 0; i < hello.length; i++) {
			tempHandshaken = tempHandshaken && reply[i] == hello[i];
		}

		return tempHandshaken;
	}

	/**
	 * Reconnect to the NXT.
	 */
	private void reconnect() {
		debug("> reconnect");
		handshaken = connected = false;
		robot.notConnected();
		try { connector.close(); } catch (IOException e) { }
		connect();
	}

	/**
	 * Send a signal as byte.
	 * @param signal to be send
	 */
	public void send(PCSignal signal) {
		int i = signal.getValue();
		byte[] b = new byte[1];
		b[0] = (byte) i;
		if (i > 127) { 
			debug("INT TO LARGE (" + i + ")."); 
		} else {
			send(b);
		}
	}

	/**
	 * Receive the next bytes.
	 * @return a DTO containing the recieved values
	 */
	private DataDTO receive() {
		byte[] b = new byte[2];
		receive(b);
		return DataDTO.parse(b);
	}

	/**
	 * Send a byte array.
	 * @param b the byte array to send
	 */
	private void send(byte[] b) {
		try {
			dataOut.write(b, 0, b.length);
			dataOut.flush();
		} catch (IOException e) {
			debug("send problem " + e);
		}
	}

	/**
	 * Read bytes from a InputStream.
	 * @param b the byte array to save the read bytes
	 * @return the total number of bytes read into the buffer, or -1 if there is no more data because the end of the stream has been reached. 
	 */
	private int receive(byte[] b) {
		try {
			return dataIn.read(b, 0, b.length);
		} catch (IOException e) {
			debug("> ERROR: Connection lost");
			reconnect();
		}
		return -1;
	}

	/**
	 * Check if the robot is connected.
	 * @return true if the robot is connected
	 */
	public boolean isConnected() {
		return connected && handshaken;
	}

	/**
	 * Start the robot.
	 */
	public void startRobot() {
		send(PCSignal.START);
	}

	/**
	 * Stop the robot.
	 */
	public void stopRobot() {
		send(PCSignal.STOP);
	}

	/**
	 * Stop the program on the robot.
	 */
	public void endProgram() {
		send(PCSignal.EXIT);
	}

	/**
	 * Reset the robot to prepare it for a new run.
	 */
	public void resetRobot() {
		send(PCSignal.RESET);
	}


	private void debug(String s) {
		if (debug ) { System.out.println(s); }
	}
}

/**
 * @author jholm
 * Parses the input signal read from the bluetooth stream.
 */
class DataDTO {

	private int data = 0;
	private RobotSignal signal = RobotSignal.NODATA;

	private DataDTO(RobotSignal signal, int data) {
		this.data = data;
		this.signal = signal;
	}

	public static DataDTO parse(byte[] b) {
		RobotSignal s = RobotSignal.intToSignal((int) b[0]);
		int d = (int) b[1];
		if (d < 0) {
			d += 256;
		}
		return new DataDTO(s, d);
	}

	public int getData() {
		return data;
	}

	public RobotSignal getSignal() {
		return signal;
	}
}
