package robot;

import main.Constants;
import main.GUI;

public class RespawnTimerThread extends Thread {

	private GUI gui;
	private Prey robot;
	private State state = State.RUNNING;

	public RespawnTimerThread(GUI gui, Prey robot) {
		this.gui = gui;
		this.robot = robot;
		this.setDaemon(true);
		this.start();
	}

	@Override
	public void run() {
		int t = Constants.DEATH_TIME;
		while (t > 0 && state != State.STOPPED) {
			if (state == State.RUNNING) {
				gui.setRespawn(robot.getRobotInfo(), t);
				t--;
			}
			try { Thread.sleep(1000); } catch (InterruptedException e) { e.printStackTrace(); }
		}
		gui.setRespawn(robot.getRobotInfo(), 0);
		robot.respawnTimerDone();
	}

	public void stopTimer() {
		state = State.STOPPED;
	}

	public void pause() {
		state = State.PAUSED;
	}

	public void unpause() {
		state = State.RUNNING;
	}
	
	enum State {
		RUNNING, PAUSED, STOPPED
	}
}
