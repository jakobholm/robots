package robot;

import main.Enum.RobotInfo;
import main.Enum.RobotState;
import main.GUI;
import main.GameMaster;
import main.ThreadException;

public class Prey extends Robot {

	private int deaths = 0;
	private int score = 0;
	private RespawnTimerThread respawnTimerThread;
	
	public Prey(RobotInfo robot, GUI gui) throws ThreadException {
		super(robot, gui);
	}
	
	public void resetRobot() {
		super.resetRobot();
		if (respawnTimerThread != null) { respawnTimerThread.stopTimer(); }
		resetScore();	
	}

	public void stopRobot() {
		super.stopRobot();
		if (respawnTimerThread != null) { respawnTimerThread.stopTimer(); }
	}
	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public int getDeaths() {
		return deaths;
	}
	
	public void dead(int energy) {
		if (state != RobotState.DEAD) {
			changeStatus(RobotState.DEAD);
			
			// increment deaths
			deaths++;
			
			// add to game score
			GameMaster.addToGameScore(energy);
			
			// reset energy
			setEnergy(0);
			
			// start respawn timer
			respawnTimerThread = new RespawnTimerThread(gui, this);
		}
	}

	public void respawnTimerDone() {
		// if the robot is idle, the game must be done
		if (state != RobotState.IDLE) { 
			changeStatus(RobotState.RUNNING);
			communication.startRobot();
		}
		respawnTimerThread = null;
	}

	public void setEnergy(int energy) {
		gui.setEnergy(robot, energy);
	}

	public void incrementScore() {
		score++;
		gui.setScore(robot, score);
	}

	public void resetScore() {
		deaths = score = 0;
//		gui.setScore(robot, score);
		gui.setEnergy(robot, 0);
	}

}
