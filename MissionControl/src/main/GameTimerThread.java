package main;

public class GameTimerThread extends Thread {

	private GUI gui;
	private GameMaster gameMaster;
	private State state;
	int time;

	public GameTimerThread(GUI gui, GameMaster gameMaster) {
		this.gui = gui;
		this.gameMaster = gameMaster;
		this.setDaemon(true);
		this.start();
	}

	public void run() {
		state = State.RUNNING;
		time = Constants.GAME_TIME;
		while (time > 0 && state != State.STOPPED) {
			if (state == State.RUNNING) {
				gui.setGameTime(time);
				time--;
			}
			try { Thread.sleep(1000); } catch (InterruptedException e) { e.printStackTrace(); }
		}
		if (state != State.STOPPED) {
			gameMaster.gameTimerDone();
		}
	}

	public void stopTimer() {
		gui.setGameTime(time); 
		state = State.STOPPED;
	}

	public void pause() {
		state = State.PAUSED;
		gui.setGameTime(time);
	}

	public void unpause() {
		state = State.RUNNING;
		gui.setGameTime(time);
	}

	private enum State {
		RUNNING, PAUSED, STOPPED;
	}
}
