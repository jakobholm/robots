package main;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.Border;

import main.Enum.RobotInfo;

public class GUI extends JFrame {

	private static final long serialVersionUID = 1L;

	private Map<String, JLabel> labelMap = new HashMap<String, JLabel>();

	private GameMaster gameMaster;
	private final String fontType = "36 days ago BRK";
	
	private final Font robotFont = new Font(fontType, Font.BOLD, 38);
	private final Font highScoreFont = new Font(fontType, Font.BOLD, 24);
	private final Font gameHFont = new Font(fontType, Font.BOLD, 32);
	private final Font gameValFont = new Font(fontType, Font.BOLD, 40);
	private final Font titleFont = new Font(fontType, Font.BOLD, 36);
	private final Font bigTitleFont = new Font(fontType, Font.BOLD, 75);
	private final Font howToFont = new Font(fontType, Font.BOLD, 40);
	private final Font pressStartFont = new Font(fontType, Font.BOLD, 50);
	
	private Container gameContainer;
	private Container pauseContainer;

	private final Border labelBorder = null;


	public GUI(String name, GameMaster gameMaster) {		
		super("Whack-A-Robot");
		this.gameMaster = gameMaster;
		
		createGUI();
		showPauseScreen();	
		setVisible(true);
	}

	private void createGUI() {
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		addWindowListener(new ClosingWindowListener(gameMaster));
		addMouseListener(new ExitMouseListener(gameMaster));
		setUndecorated(true);

		addComponentsToGameContainer();
		addComponentsToPauseContainer();
		
		//		setExtendedState(JFrame.MAXIMIZED_BOTH);
		//		pack();
		setSize(1280, 800);
	}
	
	
	public void showGameScreen() {
		try {
			setContentPane(new JComponent() {
				private static final long serialVersionUID = 1L;
				private Image image = ImageIO.read(new File("icons/foggyLeaf.jpg"));
			   
			    @Override
			    protected void paintComponent(Graphics g) {
			        g.drawImage(image, 0, 0, null);
			    }
			});
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		encapsulateContainer(gameContainer);
	}
	
	
	public void showPauseScreen() {
		try {
			setContentPane(new JComponent() {
				private static final long serialVersionUID = 1L;
				private Image image = ImageIO.read(new File("icons/foggyLeaf2.jpg"));

			    @Override
			    protected void paintComponent(Graphics g) {
			        g.drawImage(image, 0, 0, null);
			    }
			});
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		encapsulateContainer(pauseContainer);
	}
	
	public String saveName(int score) {
		setGameTime(0);
		String s = "";
		String title = "Skriv dit navn";
		String message = "Din score er " + score + ". \r\nSkriv dit navn";
		do {
			s = (String) JOptionPane.showInputDialog(this, message, title, JOptionPane.PLAIN_MESSAGE, null, null, null);
		} while (s == null);
		return s.trim();
	}

	private void addComponentsToGameContainer() {
		// set up main container
		gameContainer = new Container();
		gameContainer.setLayout(new BorderLayout());
		
		// create title panel
		gameContainer.add(createTitlePanel(), BorderLayout.NORTH);

		// create center panel
		gameContainer.add(createCenterPanel(), BorderLayout.CENTER);

		// create high score panel
		gameContainer.add(createHighScorePanel(), BorderLayout.EAST);
	}
	
	private void addComponentsToPauseContainer() {
		pauseContainer = new Container();
		pauseContainer.setLayout(new BorderLayout());
		
		// create title panel
		pauseContainer.add(createBigTitlePanel(), BorderLayout.NORTH);


		// create howto label
		pauseContainer.add(createHowToPanel(), BorderLayout.CENTER);
		
		
		// crate press to start label
		pauseContainer.add(createStartPanel(), BorderLayout.SOUTH);
	}

	private Component createBigTitlePanel() {
		JPanel panel = new JPanel(new FlowLayout());
		panel.setOpaque(false);
		JLabel label = new JLabel(Constants.TITLE);
		label.setFont(bigTitleFont);
		label.setHorizontalAlignment(JLabel.CENTER);
//		label.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		label.setPreferredSize(new Dimension(1000, 150));			
		panel.add(label);

		JPanel tempPanel = new JPanel();
		tempPanel.setOpaque(false);
//		tempPanel.setBorder(BorderFactory.createLineBorder(Color.black));
		tempPanel.add(panel);
		return tempPanel;
	}
	
	private Component createHowToPanel() {
		JPanel panel = new JPanel(new FlowLayout());
		panel.setOpaque(false);
		JLabel label = new JLabel(Constants.HOWTO_TEXT);
		label.setFont(howToFont);
		label.setHorizontalAlignment(JLabel.CENTER);
//		label.setBorder(BorderFactory.createLineBorder(Color.BLACK));
				
		label.setPreferredSize(new Dimension(850, 400));		
		panel.add(label);

		JPanel tempPanel = new JPanel();
		tempPanel.setOpaque(false);
//		tempPanel.setBorder(BorderFactory.createLineBorder(Color.black));
		tempPanel.add(panel);
		return tempPanel;
	}
	
	private Component createStartPanel() {
		JPanel panel = new JPanel(new FlowLayout());
		panel.setOpaque(false);
		JLabel label = new JLabel(Constants.TIME_LABEL_READY_TEXT);
		label.setFont(pressStartFont);
		label.setHorizontalAlignment(JLabel.CENTER);
//		label.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		label.setPreferredSize(new Dimension(1000, 150));	
		panel.add(label);

		JPanel tempPanel = new JPanel();
		tempPanel.setOpaque(false);
//		tempPanel.setBorder(BorderFactory.createLineBorder(Color.black));
		tempPanel.add(panel);
		return tempPanel;
	}
	
	private Component createTitlePanel() {
		JPanel panel = new JPanel(new FlowLayout());
		panel.setOpaque(false);
		JLabel label = new JLabel(Constants.TITLE);
		label.setFont(titleFont);
		label.setHorizontalAlignment(JLabel.CENTER);
		//		label.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		panel.add(label);

		JPanel tempPanel = new JPanel();
		tempPanel.setOpaque(false);
		//		tempPanel.setBorder(BorderFactory.createLineBorder(Color.black));
		tempPanel.add(panel);
		return tempPanel;
	}
	
	
	private JPanel createCenterPanel() {
		JPanel panel = new JPanel();
		panel.setOpaque(false);
//		panel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		panel.add(createPreyPanel());
		panel.add(createGamePanel());
	
		return panel;
	}

	/*
	 * Puts empty panels around the container, to add space around the container.
	 */
	private void encapsulateContainer(Container container) {
		Container content = getContentPane();
		content.setLayout(new BorderLayout());
		JPanel buffer = new JPanel();
		buffer.setOpaque(false);
		content.add(buffer, BorderLayout.NORTH);
		content.add(buffer, BorderLayout.SOUTH);
		content.add(buffer, BorderLayout.WEST);
		content.add(buffer, BorderLayout.EAST);
		content.add(container, BorderLayout.CENTER);
	}

	private JPanel createHighScorePanel() {
		JPanel panel = new JPanel(new GridLayout(Constants.HIGH_SCORE_LIST_SIZE, 2, 1, 1));
		panel.setOpaque(false);
		panel.setBorder(BorderFactory.createLineBorder(Color.black));
		for (int i = 1 ; i <= Constants.HIGH_SCORE_LIST_SIZE ; i++) {
			panel.add(createHighScoreNameLabel(i));
			panel.add(createHighScoreLabel(i));
		}
		JPanel tempPanel = new JPanel();
		tempPanel.setOpaque(false);
//		tempPanel.setBorder(BorderFactory.createLineBorder(Color.black));
		tempPanel.add(panel);
		return tempPanel;
	}

	private JLabel createHighScoreNameLabel(int place) {
		JLabel label = new JLabel();
		label.setPreferredSize(new Dimension(200, 60));
		label.setBorder(labelBorder);
		label.setFont(highScoreFont);
		label.setHorizontalAlignment(JLabel.CENTER);
		labelMap.put(place + Constants.HIGH_SCORE_NAME_LABEL, label);
		return label;
	}

	private JLabel createHighScoreLabel(int place) {
		JLabel label = new JLabel();
		label.setPreferredSize(new Dimension(10, 60));
		label.setBorder(labelBorder);
		label.setFont(highScoreFont);
		label.setHorizontalAlignment(JLabel.CENTER);
		labelMap.put(place + Constants.HIGH_SCORE_LABEL, label);
		return label;
	}

	private JPanel createGamePanel() {
		JPanel rootPanel = new JPanel(new FlowLayout());
		rootPanel.setOpaque(false);
//		rootPanel.setBorder(BorderFactory.createLineBorder(Color.black));
		
		
		JPanel scorePanel = new JPanel();
		scorePanel.setOpaque(false);
		scorePanel.setPreferredSize(new Dimension(414, 84));
		JLabel scoreHLabel = new JLabel("Point :");
		scoreHLabel.setPreferredSize(new Dimension(90, 70));
		scoreHLabel.setFont(gameHFont);
		JLabel scoreLabel = new JLabel("0");
		scoreLabel.setPreferredSize(new Dimension(150, 70));
		scoreLabel.setHorizontalAlignment(JLabel.CENTER);
		scoreLabel.setFont(gameValFont);
		scorePanel.add(scoreHLabel);
		scorePanel.add(scoreLabel);
		scorePanel.setBorder(BorderFactory.createLineBorder(Color.black));
		
		labelMap.put(Constants.SCORE_LABEL, scoreLabel);
		rootPanel.add(scorePanel);
		
		JPanel timePanel = new JPanel();
		timePanel.setOpaque(false);
		timePanel.setPreferredSize(new Dimension(414, 84));
		JLabel timeHLabel = new JLabel("Tid tilbage :");
		timeHLabel.setPreferredSize(new Dimension(150, 70));
		timeHLabel.setFont(gameHFont);
		JLabel timeLabel = new JLabel("0");
		timeLabel.setPreferredSize(new Dimension(100, 70));
		timeLabel.setHorizontalAlignment(JLabel.CENTER);
		timeLabel.setFont(gameValFont);
		timePanel.add(timeHLabel);
		timePanel.add(timeLabel);
		timePanel.setBorder(BorderFactory.createLineBorder(Color.black));
		
		labelMap.put(Constants.TIME_LABEL, timeLabel);
		rootPanel.add(timePanel);

		JPanel tempPanel = new JPanel();
		tempPanel.setOpaque(false);
		tempPanel.add(rootPanel);
		return tempPanel;
	}
	

	private JPanel createPreyPanel() {
		JPanel panel = new JPanel(new BorderLayout());
		panel.setOpaque(false);
		
		JPanel headerPanel = new JPanel(new GridLayout(1, 3, 1, 1));
		headerPanel.setOpaque(false);
		createHeaders(headerPanel);
		panel.add(headerPanel, BorderLayout.NORTH);
		
		JPanel preyPanel = new JPanel(new GridLayout(4, 3, 1, 1));
		preyPanel.setOpaque(false);
		for (RobotInfo r : RobotInfo.values()) {
			if(r != RobotInfo.PREDATOR) {
				createPreyLabel(preyPanel, r);
			}
		}
		panel.add(preyPanel, BorderLayout.SOUTH);

		JPanel tempPanel = new JPanel();
		tempPanel.setOpaque(false);
		tempPanel.setBorder(BorderFactory.createLineBorder(Color.black));
		tempPanel.add(panel);
		return tempPanel;
	}

	private void createPreyLabel(JPanel panel, RobotInfo robot) {
		panel.add(createIconLabel(robot));
		panel.add(createEnergyLabel(robot));
		panel.add(createStatusLabel(robot));
	}

	private JLabel createHeader(String text) {
		JLabel label = new JLabel(text);
		label.setPreferredSize(new Dimension(200, 50));
		label.setBorder(labelBorder);
		label.setFont(robotFont);
		label.setHorizontalAlignment(JLabel.CENTER);
		label.setVerticalAlignment(JLabel.CENTER);
		return label;
	}

	private void createHeaders(JPanel panel) {
		panel.add(createHeader(""));
		panel.add(createHeader(Constants.ROBOT_HEADER_ENERGY));
		panel.add(createHeader(""));
	}

	private JLabel createEnergyLabel(RobotInfo robot) {
		JLabel label = new JLabel("0");
		label.setBorder(labelBorder);
		label.setFont(robotFont);
		label.setHorizontalAlignment(JLabel.CENTER);
		labelMap.put(robot + Constants.ENERGY_LABEL, label);
		return label;

	}

	private JLabel createStatusLabel(RobotInfo robot) {
		JLabel label = new JLabel();
		label.setBorder(labelBorder);
		label.setFont(robotFont);
		label.setIcon(new ImageIcon(Constants.NOT_CONNECTED_ICON));
		label.setHorizontalAlignment(JLabel.CENTER);
		
		labelMap.put(robot + Constants.STATUS_LABEL, label);
		return label;
	}

	private JLabel createIconLabel(RobotInfo robot) {
		JLabel label = new JLabel();
		label.setPreferredSize(new Dimension(273, 127));
		label.setBorder(labelBorder);
		label.setIcon(new ImageIcon(robot.getIcon()));
		label.setHorizontalAlignment(JLabel.CENTER);
//		labelMap.put(robot + Constants.STATUS_LABEL, label);
		return label;
	}

	/*************************************************************************/

	public void setScore(RobotInfo robot, int value) {
		setLabelText(robot, Constants.SCORE_LABEL, "" + value, (value > 0));
	}

	public void setRespawn(RobotInfo robot, int value) {
		String res = (value <= 0 ? "" : "" + value);
		setLabelText(robot, Constants.STATUS_LABEL, res);
	}

//	public void setIcon(RobotInfo robot, String path) {
//		Icon icon = new ImageIcon(path);
//		JLabel label = labelMap.get(robot.toString() + Constants.STATUS_LABEL);
//		label.setIcon(icon);
//	}

	private void setLabelText(RobotInfo robot, String boxType, String value) {
		setLabelText(robot, boxType, value, false);
	}

	private void setLabelText(RobotInfo robot, String boxType, String value, boolean animation) {
		JLabel label = labelMap.get(robot + boxType);
		label.setText("" + value);

		if (animation) {
			new AnimationThread(label);
		}
	}

	public void setGameTime(int time) {
		JLabel label = labelMap.get(Constants.TIME_LABEL);
		switch (gameMaster.getState()) {
		case RUNNING:
			//label.setBorder(BorderFactory.createLineBorder(Color.black));
			label.setText("" + time);
			if (time <= 10) {
				new AnimationThread(label);
			}
			break;
		case READY:
			label.setText(Constants.TIME_LABEL_READY_TEXT);
		default:
			break;
		}
	}

	public void setEnergy(RobotInfo robot, int energy) {
		JLabel label = labelMap.get(robot + Constants.ENERGY_LABEL);
		label.setText("" + energy);
	}

	public void setHighScore(int place, ScoreDTO scoreDTO) {
		JLabel labelName = labelMap.get(place + Constants.HIGH_SCORE_NAME_LABEL);
		JLabel labelScore = labelMap.get(place + Constants.HIGH_SCORE_LABEL);
		labelName.setText(scoreDTO.getName());
		labelScore.setText("" + scoreDTO.getScore());
	}

	public void setGameScore(int gameScore) {
		JLabel label = labelMap.get(Constants.SCORE_LABEL);
		label.setText("" + gameScore);
		new AnimationThread(label);
	}

	public void setRobotStatusIcon(RobotInfo robot, String icon) {
		JLabel label = labelMap.get(robot.toString() + Constants.STATUS_LABEL);
		if (icon == null || icon.equals("")) {
			label.setIcon(null);
		} else {
			label.setIcon(new ImageIcon(icon));
		}
	}

	class AnimationThread extends Thread {
		private JLabel label;
		public final Font animationFont = new Font(fontType, Font.BOLD, 50);
	
		public AnimationThread(JLabel label) {
			this.label = label;
			start();
		}
	
		public void run() {
			label.setFont(animationFont);
			try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
	
			label.setFont(gameValFont);
	
		}
	}
}

class ClosingWindowListener extends WindowAdapter {

	private GameMaster gameMaster;

	public ClosingWindowListener(GameMaster gameMaster) {
		this.gameMaster = gameMaster;
	}

	@Override
	public void windowClosing(WindowEvent e) {
		gameMaster.quit();
	}
}

class ExitMouseListener implements MouseListener {

	private GameMaster gameMaster;
	private int maxY;

	public ExitMouseListener(GameMaster gameMaster) {
		this.gameMaster = gameMaster;
		Toolkit toolkit =  Toolkit.getDefaultToolkit ();
		Dimension dim = toolkit.getScreenSize();
		maxY = (int) dim.getHeight();
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		int yPerc = 100*e.getY()/maxY;
		
		if (yPerc < 10) {
			gameMaster.quit();
		}
	}

	@Override
	public void mouseEntered(MouseEvent arg0) { }

	@Override
	public void mouseExited(MouseEvent arg0) { }

	@Override
	public void mousePressed(MouseEvent arg0) { }

	@Override
	public void mouseReleased(MouseEvent arg0) { }
}

class ImagePanel extends JComponent {
	private static final long serialVersionUID = 1L;
	private Image image;
    public ImagePanel(Image image) {
        this.image = image;
    }
    @Override
    protected void paintComponent(Graphics g) {
        g.drawImage(image, 0, 0, null);
    }
}

