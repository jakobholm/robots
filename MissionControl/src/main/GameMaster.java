package main;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import main.Enum.Difficulty;
import main.Enum.GameState;
import main.Enum.RobotInfo;
import main.Signal.PCSignal;

import robot.Robot;
import robot.Predator;
import robot.Prey;


public class GameMaster {

	private static Predator predator = null;
	private Map<RobotInfo, Robot> robots = new HashMap<RobotInfo, Robot>();
	private GameState state = GameState.READY;
	private GameTimerThread gameTimerThread;
	private static GUI gui;
	private FileHandler fileHandler;
	private boolean debug = false;
	private Difficulty difficulty = Difficulty.HARD;
	private static volatile int gameScore = 0;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new GameMaster();
	}

	/**
	 * Creates the GUI, reads the high scores, starts all robots and serves as the main loop.
	 */
	public GameMaster() {
		debug("Game started.");

		gui = new GUI("GUI", this);
		gui.addKeyListener(new ControllerKeyListener(this));

		readHighScores();

		startThreads();

		// main loop after connecting to threads
		// runs until the method quit() sets state to QUIT
		while (state != GameState.QUITTING) {
			try { Thread.sleep(200); } catch (InterruptedException e) {}
		}

		// making sure all communication threads are done
		try { Thread.sleep(100); } catch (InterruptedException e) {}

		// this makes sure the gui also closes
		System.exit(0);
	}

	/**
	 * Reads the high scores from a file.
	 */
	private void readHighScores() {
		debug("> readHighScores");
		fileHandler = new FileHandler();
		List<ScoreDTO> scores = fileHandler.getScores();
		ScoreDTO current;

		for (int i = 1 ; i <= (Math.min(Constants.HIGH_SCORE_LIST_SIZE, scores.size())) ; i++) {
			current = scores.get(i-1);
			gui.setHighScore(i, current); 
		}
	}

	/**
	 * Saves the score
	 * @param name
	 * @param score
	 */
	private void saveHighScore(String name, int score) {
		if (name.equals("")) {
			return;
		}
		debug("> saveHighScore");
		fileHandler.appendToFile(name, score);
		readHighScores();
	}

	/**
	 * Starts all threads.
	 */
	private void startThreads() {
		debug("> startThreads.");
		try {
			robots.put(RobotInfo.PREDATOR, new Predator(RobotInfo.PREDATOR, gui));
			robots.put(RobotInfo.DONATELLO, new Prey(RobotInfo.DONATELLO, gui));
			robots.put(RobotInfo.MICHELANGELO, new Prey(RobotInfo.MICHELANGELO, gui));
			robots.put(RobotInfo.RAPHAEL, new Prey(RobotInfo.RAPHAEL, gui));
			robots.put(RobotInfo.LEONARDO, new Prey(RobotInfo.LEONARDO, gui));
			debug("    Threads running.");
		} catch (ThreadException e) {
			e.printStackTrace();
		}
		if (robots.containsKey(RobotInfo.PREDATOR)) {
			predator = (Predator) robots.get(RobotInfo.PREDATOR);
		}
	}
	
	/**
	 * Starts a new game.
	 */
	public void startGame() {
		debug("> startGame");
		
		resetGame();
		
		if (state == GameState.READY) {
			state = GameState.RUNNING;
			Robot r;
			for(RobotInfo robot : robots.keySet()) {
				r = robots.get(robot);
				r.startRobot();
				r.setDifficulty(difficulty);
			}

			gameTimerThread = new GameTimerThread(gui, this);
			gui.showGameScreen();
		}
	}

	/**
	 * Stop the game before time has run out.
	 */
	public void stopGame() {
		debug("> stopGame");
		if (state == GameState.RUNNING) {
			state = GameState.READY; 
			gameTimerThread.stopTimer();
			
			Robot r;
			for(RobotInfo robot : robots.keySet()) {
				r = robots.get(robot);
				r.stopRobot();
			}
			gui.showPauseScreen();
		}
	}
	
	/**
	 * 
	 */
	public void resetGame() {
		debug("> resetGame");
		gameScore = 0;
		if (state == GameState.RUNNING) {
			stopGame();
		}
		
		Robot r;
		for(RobotInfo robot : robots.keySet()) {
			r = robots.get(robot);
			r.resetRobot();
		}
		
		
	}


	/**
	 * Called by the GameTimerThread when the time has run out.
	 */
	public void gameTimerDone() {
		debug("> gameTimerDone");
		stopGame();

//		int score = 0;

//		Robot r;
//		for(RobotInfo robot : robots.keySet()) {
//			r = robots.get(robot);
//			if (robot == RobotInfo.PREDATOR) {
//				score -= r.getScore();
//			} else {
//				score -= r.getScore();
//				score += 100*r.getDeaths();
//			}
//		}
		
		if (gameScore > 0) {
			saveHighScore(gui.saveName(gameScore), gameScore);
		}
		resetGame();
	}

	/**
	 * Ends the program on all robots.
	 */
	private void turnOffAllRobots() {
		debug("> turnOffAllRobots");
		Robot r;
		for(RobotInfo robot : robots.keySet()) {
			r = robots.get(robot);
			if (r.isConnected()) {
				debug("    " + robot);
				r.endProgram();
			}
		}
	}

	/**
	 * Quits the program on the pc and on all robots.
	 */
	public void quit() {
		debug("> quit");
		stopGame();
		turnOffAllRobots();
		state = GameState.QUITTING;
	}

	/**
	 * @return the state of the robot
	 */
	public GameState getState() {
		return state;
	}

	private void debug(String s) {
		if (debug) {
			System.out.println(s);
		}
	}
	
	public void executePredatorCommand(PCSignal signal) {
		if (predator != null) {
			predator.executePredatorCommand(signal);
		}
		
	}

	public Set<RobotInfo> getRobots() {
		return robots.keySet();
	}

	public static void addToGameScore(int score) {
		gameScore += score;
		gui.setGameScore(gameScore);
	}
}
