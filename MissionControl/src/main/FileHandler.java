package main;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Random;


public class FileHandler {

	private BufferedReader br;
	private BufferedWriter bw;

	private List<ScoreDTO> scores;

	public FileHandler() {
		openFile();
		readFile();
	}

	private void openFile() {
		try {
			new File(Constants.HIGH_SCORE_FILE_NAME).createNewFile();

			FileReader fr = new FileReader(Constants.HIGH_SCORE_FILE_NAME);
			br = new BufferedReader(fr);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void appendToFile(String name, int score) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date dated = new Date();
		String date = dateFormat.format(dated);
		try {
			FileWriter fw = new FileWriter(Constants.HIGH_SCORE_FILE_NAME, true);
			bw = new BufferedWriter(fw);
			bw.write(ScoreDTO.writeable(name, score, date) + "\r\n");
			//			bw.flush();
			bw.close(); // closes and flushes
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void readFile() {
		ScoreDTO line;
		scores = new ArrayList<ScoreDTO>();
		while ((line = readLine()) != null) {
			scores.add(line);
		}
	}

	private ScoreDTO readLine() {
		try {
			return ScoreDTO.parse(br.readLine());
		} catch (IOException e) {
			return null;
		}
	}

	public List<ScoreDTO> getScores() {
		Collections.sort(scores);
		return scores;
	}
}

class ScoreDTO implements Comparable<ScoreDTO> {

	private String name;
	private int score;
	private String date;

	private final static String DELIMITER = ";";

	private ScoreDTO(String name, int score, String date) {
		this.name = name;
		this.score = score;
		this.date = date;
	}

	public static ScoreDTO parse(String line) {
		if (line == null) { return null; }
		String[] a = line.split(DELIMITER);
		if (a.length < 3) {
			return null;
		} else {
			return new ScoreDTO(a[0].trim(), Integer.parseInt(a[1].trim()), a[2].trim());
		}
	}

	public static String writeable(String name, int score, String date) {
		return new ScoreDTO(name.trim(), score, date.trim()).toString();
	}

	public String getName() {
		return name;
	}

	public int getScore() {
		return score;
	}

	public String getDate() {
		return date;
	}

	@Override
	public int compareTo(ScoreDTO obj) {
		if (obj.score == score) {
			return new Random().nextInt(2)-1;
		}
		return obj.score - score;
	}

	public String toString() {
		return name + DELIMITER + score + DELIMITER + date;
	}
}
