package main;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import main.Signal.PCSignal;

public class ControllerKeyListener implements KeyListener {

	private GameMaster gameMaster;
	private ArrayList<Integer> pressedKeys = new ArrayList<>(); 
	
	private final boolean DEBUG = true;

	public ControllerKeyListener(GameMaster gameMaster) {
		this.gameMaster = gameMaster;
	}

	@Override
	public void keyPressed(KeyEvent e) {
		int keyCode = e.getKeyCode();
		
		if (!contains(Key.getKey(keyCode))) {
			debug("  # keyPressed: " + keyCode + " - " + KeyEvent.getKeyText(keyCode));
		}

		if (Key.keyExists(keyCode) && !contains(Key.getKey(keyCode))) {
			pressedKeys.add(keyCode);
			switch (Key.getKey(keyCode)) {
			case START_STOP:
				switch (gameMaster.getState()) {
				case READY:
					gameMaster.startGame();
					break;
				case RUNNING:
					gameMaster.stopGame();
					break;
				default:
					break;
				}
				break;
//			case START:
//				gameMaster.startGame();
//				break;
//			case STOP:
//				gameMaster.stopGame();
//				break;
//			case EXIT_1:
//				exit_1 = true;
//				if (exit_1 && exit_2) {
////					gameMaster.quit();
//				}
//				break;
//			case EXIT_2:
//				exit_2 = true;
//				if (exit_1 && exit_2) {
////					gameMaster.quit();
//				}
//				break;
//			case EXIT:
//				gameMaster.quit();
//				break;
//			case RESET: 
//				gameMaster.resetGame();
//				break;
			case ATTACK:
				gameMaster.executePredatorCommand(PCSignal.ATTACK);
				break;
			case DOWN:
			case LEFT:
			case RIGHT:
			case UP:
				PCSignal s = getMoveSignal();
				debug("> signal: " + s);
				gameMaster.executePredatorCommand(s);
				break;
			default:
				break;
			}
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		debug("  # keyReleased: " + e.getKeyCode() + " - " + KeyEvent.getKeyText(e.getKeyCode()));
		int keyCode = e.getKeyCode();

		if (Key.keyExists(keyCode)) {
			pressedKeys.remove((Integer)keyCode);

			switch (Key.getKey(keyCode)) {
			case DOWN:
			case UP:
			case LEFT:
			case RIGHT:
				PCSignal s = getMoveSignal();
				debug("< signal: " + s);
				gameMaster.executePredatorCommand(s);
				break;
//			case EXIT_1:
//				exit_1 = false;
//				break;
//			case EXIT_2:
//				exit_2 = false;
//				break;
			default:
				break;
			}
		}
	}


	private PCSignal getMoveSignal() {

		// Stop
		if (((contains(Key.UP) && contains(Key.DOWN)) || (!contains(Key.UP) && !contains(Key.DOWN))) && 
				((contains(Key.RIGHT) && contains(Key.LEFT)) || (!contains(Key.RIGHT) && !contains(Key.LEFT)))) {
			return PCSignal.STOP_MOVING;
		}

		// Turn on the spot
		if (!contains(Key.DOWN) && !contains(Key.UP)) {

			if (contains(Key.LEFT)) {
				return PCSignal.LEFT;
			} else if (contains(Key.RIGHT)) {
				return PCSignal.RIGHT;
			}
		}

		// Forward, forward_left and right
		if (contains(Key.UP) && !contains(Key.DOWN)) {
			if (contains(Key.LEFT) && !contains(Key.RIGHT)) {
				return PCSignal.FORWARD_LEFT;
			} else if (!contains(Key.LEFT) && contains(Key.RIGHT)) {
				return PCSignal.FORWARD_RIGHT;
			} else {
				return PCSignal.FORWARD;
			}
		}

		// Backwards, backward_left and right
		if (contains(Key.DOWN) && !contains(Key.UP)) {
			if (contains(Key.LEFT) && !contains(Key.RIGHT)) {
				return PCSignal.BACKWARD_LEFT;
			} else if (!contains(Key.LEFT) && contains(Key.RIGHT)) {
				return PCSignal.BACKWARD_RIGHT;
			} else {
				return PCSignal.BACKWARD;
			}
		}

		return PCSignal.NODATA;
	}

	private boolean contains(Key key) {
		if (key == null) { return false; }
		return pressedKeys.contains((Integer) key.getValue());
	}

	@Override
	public void keyTyped(KeyEvent e) {

	}


	public enum Key {
		START_STOP(10), // enter
//		START(10), // s
//		STOP(84), // t
//		RESET(82), // r
//		EXIT(27), // escape
//		EXIT_1(112), // F1
//		EXIT_2(121), // F10
		LEFT(37),
		UP(38),
		RIGHT(39),
		DOWN(12),
		ATTACK(32); // space

		private final int value;

		Key(int value) {
			this.value = value;
		}

		public int getValue() {
			return value;
		}

		private static final Map<Integer, Key> intToKeyMap = new HashMap<Integer, Key>();
		static {
			for (Key type : Key.values()) {
				intToKeyMap.put(type.value, type);
			}
		}

		public static Key getKey(int value) {
			if (intToKeyMap.containsKey(value)) {
			Key type = intToKeyMap.get(Integer.valueOf(value));
			return type;
			}
			return null;
		}

		public static boolean keyExists(int value) {
			return intToKeyMap.containsKey(new Integer(value));
		}

		public String toString() {
			String s = super.toString();
			return s.substring(0, 1) + s.substring(1).toLowerCase();
		}
	}
	
	private void debug(String s) {
		if (DEBUG) {
			System.out.println(s);
		}
	}
}
