package main;
public class Constants {
	
	public static final String SCORE_LABEL = "ScoreLabel";
	public static final String STATUS_LABEL = "StatusLabel";
	public static final String RESPAWN_LABEL = "RespawnLabel";
	public static final String TIME_LABEL = "TimeLabel";
	public static final String ENERGY_LABEL = "EnergyLabel";
	public static final String HIGH_SCORE_LABEL = "HighScoreLabel";
	public static final String HIGH_SCORE_NAME_LABEL = "HighScoreNameLabel";
	
	public static final String TITLE = "Whack-A-Robot";
	public static final String TIME_LABEL_READY_TEXT = "<html>Tryk p&aring; ENTER for at starte</html>";
	public static final String HOWTO_TEXT = "<html>Du styrer en myre, der jagter bladlus. <img width='80' height='69' src='file:icons/bladlus_generic_icon.png'/> " +
											"<br>For at f� bladlusenes opsamlede energi skal du sl&aring; dem i hovedet. " +  
											"<br><br>Myren styres med piletasterne og du sl&aring;r ved at trykke p� mellemrumstasten.</html>";
	
	public static final String ROBOT_HEADER_NAME   = "Navn";
	public static final String ROBOT_HEADER_SCORE  = "Point";
	public static final String ROBOT_HEADER_STATUS = "Status";
	public static final String ROBOT_HEADER_ENERGY = "Energi";
	public static final String ROBOT_HEADER_REVIVE = "Genopliv";
	
	public static final int DEATH_TIME = 10;
	public static final int GAME_TIME = 120;
	
	public static final String HIGH_SCORE_FILE_NAME = "highscorefile.txt";
	public static final int HIGH_SCORE_LIST_SIZE = 11;

	public static final String NOT_CONNECTED_ICON = "icons/not_connected.png";
	public static final String NOT_HANDSHAKEN_ICON = "icons/not_handshaken.png";
	
	public static final String LEONARDO_RUNNING_ICON = "icons/Bladlus_rod.png";
	public static final String MICHELANGELO_RUNNING_ICON = "icons/Bladlus_hvid.png";
	public static final String DONATELLO_RUNNING_ICON = "icons/Bladlus_sort.png";
	public static final String RAPHAEL_RUNNING_ICON = "icons/Bladlus_gul.png";
	public static final String PREDATOR_RUNNING_ICON = "icons/gears.png";
//	public static final String PREDATOR_HIT_ICON = "icons/predator_hit.png";
//	public static final String NOT_HANDSHAKEN = "!";
//	public static final String CONNECTED = "�";
}