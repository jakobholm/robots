package main;

import java.awt.Color;

public class Enum {
	
	public enum RobotInfo {
		LEONARDO("00:16:53:14:5e:07", Constants.LEONARDO_RUNNING_ICON, Color.BLUE), 
		RAPHAEL("00:16:53:14:47:f3", Constants.RAPHAEL_RUNNING_ICON, Color.RED), 
		DONATELLO("00:16:53:1a:2e:46", Constants.DONATELLO_RUNNING_ICON, new Color(255, 0, 255)), 
		MICHELANGELO("00:16:53:17:38:31", Constants.MICHELANGELO_RUNNING_ICON, Color.ORANGE), 
		PREDATOR("00:16:53:10:88:6D", Constants.PREDATOR_RUNNING_ICON, Color.BLACK);

		private final String address;
		private final String runningIcon;
		private final Color color;
		
		RobotInfo(String address, String runningIcon, Color color) {
			this.address = address;
			this.runningIcon = runningIcon;
			this.color = color;
		}
		
		public String getAddress() {
			return address;
		}
		
		public String getIcon() {
			return runningIcon;
		}

		public Color getColor() {
			return color;
		}

		@Override 
		public String toString() {
			String s = super.toString();
			return s.substring(0, 1) + s.substring(1).toLowerCase();
		}
	}
	
	public enum RobotState {
		DEAD("icons/skull.png"),
		RUNNING("icons/running.png"), 
		IDLE("icons/idle.png");
		
		private final String iconPath;
		
		RobotState(String iconPath) {
			this.iconPath = iconPath;
		}
		
		public String getIconPath() {
			return iconPath;
		}
		
		@Override 
		public String toString() {
			//only capitalize the first letter
			String s = super.toString();
			return s.substring(0, 1) + s.substring(1).toLowerCase();
		}
	}
	

	public enum GameState {
		RUNNING,
		READY, 
		QUITTING;
		
		@Override 
		public String toString() {
			//only capitalize the first letter
			String s = super.toString();
			return s.substring(0, 1) + s.substring(1).toLowerCase();
		}
	}

	public enum Difficulty {
		EASY, NORMAL, HARD;
	}
}
