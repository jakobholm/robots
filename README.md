# leJOS for NXT

This repository consists of the (final) source code that was originally developed for the end project for the course [Embedded Systems - Embodied Agents](http://lego.wikidot.com/) which was further developed as a study project ([Whack-a-Robot](http://whackarobot.wikidot.com/)) to be exhibited at [Danmarks Industrimuesum](http://www.industrimuseet.dk/) in Horsens.

The project itself is a game where the player controls a predator that hunts four preys with the intention of hitting the preys on the back with the predator hammer ([Youtube video](http://www.youtube.com/watch?v=AUAtcN_eeY8)).

The purpose of this repository is for others to be able to reuse some of the code, architecture or ideas, and the following will be a walk-through of the primary elements of the system. The following samples is edited so that only the key aspects of the code is shown; the complete code can still be found in the [source](https://bitbucket.org/jakobholm/robots/src/) folders.

# Communication

All of the four preys and the predator are connected to a pc via bluetooth. The pc sends start and stop game signals to all robots, the preys continually sends updates to the pc, and the pc sends movement commands to the predator whenever the user interacts. All this communication can be difficult to handle through a single bluetooth connection (on the pc), and since the predator on rare occasions could have a bit of a delay from the user interacts till the effect kicks in we suspect that this is close to the limit for a single bluetooth connection.

### PC-Side

In each bluetooth connection the PC is the master in the master-slave relationship, and so the PC tries to connect to each robot with the below [`connect`](https://bitbucket.org/jakobholm/robots/src/_??_/MissionControl/src/robot/CommunicationThread.java?at=master#cl-78) method. This method is called at startup and is called whenever the PC is unsuccessful in connecting with a robot. This is to make the system more robust in case a robot has to be restarted it will easily reconnect with the PC.

	:::java
		/**
		 * Connects to the NXT via bluetooth and performs the handshake.
		 * @return true when connection is established and handshake is correct
		 */
		private boolean connect() {
			while (true) {
				// connect and handshake
				connected = establishConnection(); // connect to a specific robot
				if (connected) {
					robot.connectedButNotHandshaken();
					handshaken = handshake();
				}

				if (connected && handshaken) {
					robot.connected(); // inform the system that the robot is connected
					return true; 
				} else {
					if (connected && !handshaken) {
						// the program is not running on the robot
						runProgramOnNXT();
					}
					// reset connection
					try { connector.close(); } catch (IOException ignored) { }
					connected = handshaken = false;
				}
			}
		}
		
### Robot-side

Once the program is running on a robot it will check to see if someone is trying to establish a connectionas shown in the below [`connect`](https://bitbucket.org/jakobholm/robots/src/_??_/Pray/src/helperclasses/Communication.java?at=master#cl-30) method. If this is the case and the handshake is succesful, a connection has been made and is ready to send and receive data. Otherwise it will try again with an interval of 2.5 seconds. 

	:::java
		/**
		 * Connects to the PC via bluetooth and performs the handshake.
		 * @return true when connection is established and handshake is correct
		 */
		private boolean connect() {
			
			// repeat this for the odd chance that someone else tries to connect.
			while (true) {
				LCD.clear();
				
				// connect and handshake
				connected = establishConnection(); // wait for someone to start a connection
				if (connected) {
					handshaken = handshake();
				}
				
				if (connected && handshaken) {
					return true;
				} else {
					// reset connection
					connection.close();
					connected = handshaken = false; 
					try { Thread.sleep(2500); } catch (InterruptedException ignored) {}
				}
			}
		}
		
### Handshake

The handshake used in this implementation is very simple. It is a simple exchange of a byte array consisting of the characters `RCP`. Upon connection the master sends `RCP` to the slave, and the slave checks that it is indeed `RCP` it received. The slave sends this back, and the master checks that it matches what it send. This is a very simple handshake but all it has to do is to make sure no one connects to the robot by accident. 

![Connection Protocol](https://bitbucket.org/jakobholm/robots/raw/master/pics/connection_protocol.png "Connection Protocol")

# Behavior Based System

![Behavior Architecture](https://bitbucket.org/jakobholm/robots/raw/master/pics/BehaviorArchitecture.png "Behavior Architecture")

### Behaviors

[`getMotivation`](https://bitbucket.org/jakobholm/robots/src/_??_/Pray/src/behaviors/AvoidObstacle.java?at=master#cl-22) from the class
[`AvoidObstacle`](https://bitbucket.org/jakobholm/robots/src/_??_/Pray/src/behaviors/AvoidObstacle.java)

	:::java
		public int getMotivation() {
			// Saves the measurement of the ultrasonic sensor. 
			int distance = Sensors.getUltraSonicDistance();
			
			if (isRunning()) {
				// If the robot is avoiding an obstacle the motivations is at least 50. 
				// This makes sure that it takes something important to take precedence. 
				return (int) Math.max(50, (detectDistance-distance)*(100/detectDistance));
			} else {
				return (int) Math.max(0,  (detectDistance-distance)*(100/detectDistance));
			}		
		}

And the [`action`](https://bitbucket.org/jakobholm/robots/src/_??_/Pray/src/behaviors/Behavior.java?at=master#cl-145) method contains the instructions to be performed by the behavior

	:::java
		public void action() throws InterruptBehaviorException {...}

For instance the `AvoidObstacle` [`action`](https://bitbucket.org/jakobholm/robots/src/_??_/Pray/src/behaviors/AvoidObstacle.java?at=master#cl-36) method backs away from the obstacle in a direction depending on the orientation of the head of the robot.

### Behavior Thread

	:::java
		public void run() {
			while (true) {
				try {
					if (currentBehavior != null) {
						currentBehavior.action();
					} else {
						try { Thread.sleep(100); } catch (InterruptedException e) { }
					}
				} catch (InterruptBehaviorException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

### Arbitrator

The job of the arbitrator is to decide which behavior to perform. Below is the [run](https://bitbucket.org/jakobholm/robots/src/_??_/Prey/src/main/Arbitrator.java?at=master#cl-26) method (i.e. the main loop) of the arbitrator, and it is all rather well documentet.

	:::java
		public void run() {
			int maxMotivation = 0; // Keeps track of the highest motivation for each run.
			int currentMotivation; // The motivation for the current behavior when running through behaviors.
			Behavior maxBehavior; // The behavior with the maximum motivation.

			while (MainProgram.getState() != GameState.QUITTING) {
				while (MainProgram.getState() != GameState.RUNNING) {
					try { Thread.sleep(100); } catch (InterruptedException e) { e.printStackTrace(); }
				}
				
				maxBehavior = defaultBehavior;
				
				for (Behavior currentBehavior : behaviors) {
					if (currentBehavior.equals(defaultBehavior)) { continue; }
					
					// Print the behavior along with the priority and motivation to the LCD.
					LCD.drawString(currentBehavior.getBehaviorPriority() + ", "	
									+ "mot" /*currentBehavior.getMotivation()*/ + ", " 
									+ currentBehavior.getName() + "      ", 
									0, currentBehavior.getBehaviorPriority());
					// Reset the maxMotivation.
					maxMotivation = maxBehavior.getMotivation();
					currentMotivation = currentBehavior.getMotivation();

					// If the new behavior has higher motivation or if two behaviors are tied for motivation and the new has higher priority.

					if (currentMotivation > maxMotivation
							|| (currentMotivation == maxMotivation && maxBehavior.getBehaviorPriority() > currentBehavior.getBehaviorPriority()) 
							&& currentMotivation > 0) {
						// A new behavior has taken the lead.
						maxBehavior = currentBehavior;
					}
				}

				// Stop runnningBehavior and starts new one, if the two aren't the same
				if (runnningBehavior == null || !maxBehavior.equals(runnningBehavior)) {
					// runnningBehavior could be null. If it is not it should be stopped.
					if (runnningBehavior != null) {
						behaviorThread.stopBehavior();
					}

					// Run the highest motivated behavior and print the name of the
					// behavior on the first line of the LCD.
					behaviorThread.startBehavior(maxBehavior);
					LCD.drawString("Running " + maxBehavior.getName() + "       ", 0, 0);

					// Prepare for the next run.
					runnningBehavior = maxBehavior;
					lastMotivation = maxMotivation;

					// Lets a behavior interrupt itself if it is selfInterruptible
					// and has higher or equal motivation
				} else if (maxBehavior.equals(runnningBehavior) && maxBehavior.isSelfInterruptible() && maxMotivation >= lastMotivation) {

					// Restart behavior.
					behaviorThread.startBehavior(maxBehavior);

					// Prepare for the next run.
					lastMotivation = maxMotivation;
				}

				// Every 10 ms the entire loop is repeated and all motivations are checked.
				try { Thread.sleep(10); } catch (InterruptedException e) { e.printStackTrace(); }
			}
		}

# Remote Controlling

