package predator;

import java.util.HashMap;
import java.util.Map;

public class Signal {
	
	public enum RobotSignal {
		// common robot signals
		NODATA(0),
		TURNING_OFF(1),
		// signals from prey
		DEATH(2),
		HUNGER(3),
		SCORE(4),
		
		// signals from predator
		HIT(5),
		
		DEBUG(6);
		
		private final int value;
		
		RobotSignal(int value) {
			this.value = value;
		}
		
		public int getValue() {
			return value;
		}
		
		private static final Map<Integer, RobotSignal> intToSignalMap = new HashMap<Integer, RobotSignal>();
		static {
		    for (RobotSignal type : RobotSignal.values()) {
		        intToSignalMap.put(type.value, type);
		    }
		}

		public static RobotSignal intToSignal(int i) {
			return intToSignalMap.get(Integer.valueOf(i));
		}
	}

	public enum PCSignal {
		// signals controlling the game
		NODATA(0),
		EXIT(1),
		START(2),
		STOP(3),
		RESET(4), 
		
		// signals controlling the predator
		ATTACK(5),
		LEFT(6),
		RIGHT(7),
		FORWARD(8),
		BACKWARD(9),
		FORWARD_LEFT(10),
		FORWARD_RIGHT(11),
		BACKWARD_LEFT(12),
		BACKWARD_RIGHT(13),
		STOP_MOVING(14);

		private final int value;
		
		PCSignal(int value) {
			this.value = value;
		}
		
		public int getValue() {
			return value;
		}
		
		private static final Map<Integer, PCSignal> intToSignalMap = new HashMap<Integer, PCSignal>();
		static {
		    for (PCSignal type : PCSignal.values()) {
		        intToSignalMap.put(type.value, type);
		    }
		}

		public static PCSignal intToSignal(int i) {
			return intToSignalMap.get(Integer.valueOf(i));
		}
	}
}