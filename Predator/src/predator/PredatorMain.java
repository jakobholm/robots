package predator;
import lejos.nxt.Button;
import lejos.nxt.ButtonListener;
import lejos.nxt.LCD;

import java.io.IOException;

import predator.Signal.PCSignal;
import predator.Signal.RobotSignal;

public class PredatorMain implements ButtonListener {
	private PredatorMover mover;

	private Communication communication;

	private State state = State.READY;

	public static void main(String[] args) throws IOException {
		new PredatorMain();
	}

	public PredatorMain() {
		Button.ESCAPE.addButtonListener(this);
		mover = new PredatorMover();
		communication = new Communication();
		mainLoop();
	}

	public void mainLoop() {
		LCD.drawString("State:  " + state + "     ", 0, 0);
		while (state != State.QUITTING) {
			LCD.drawString("State:  " + state + "     ", 0, 0);

			PCSignal signal = communication.receiveSignal();
			LCD.drawString("Signal: " + signal + "     ", 0, 1);

			executeCommand(signal);

			try { Thread.sleep(100); } catch (InterruptedException e) { }
		}
		mover.stopMoving();
	}


	/**
	 * Executes the command that is recieved from missioncontrol.
	 */
	protected void executeCommand(PCSignal s) {
		
		// If the game is not running, do not execute commands
		if (state != State.RUNNING) {
			switch (s) {
			case NODATA:
			case LEFT:
			case RIGHT:
			case ATTACK:
			case BACKWARD:
			case BACKWARD_LEFT:
			case BACKWARD_RIGHT:
			case FORWARD:
			case FORWARD_LEFT:
			case FORWARD_RIGHT:
			case STOP_MOVING:
				return;
			default:
			}
		}

		switch (s) {
		case ATTACK:
			mover.attack();
			communication.sendSignal(RobotSignal.HIT);
			break;
		case STOP:
			state = State.READY;
			mover.stopMoving();
			break;
		case START:
			state = State.RUNNING;
			break;
		case EXIT:
			state = State.QUITTING;
			break;
		case BACKWARD:
			mover.backward();
			break;
		case BACKWARD_LEFT:
			mover.backwardLeft();
			break;
		case BACKWARD_RIGHT:
			mover.backwardRight();
			break;
		case FORWARD:
			mover.forward();
			break;
		case FORWARD_LEFT:
			mover.forwardLeft();
			break;
		case FORWARD_RIGHT:
			mover.forwardRight();
			break;
		case LEFT:
			mover.turnLeft();
			break;
		case RIGHT:
			mover.turnRight();
			break;
		case STOP_MOVING:
			mover.stopMoving();
			break;
		default:
			break;
		}
	}


	@Override
	public void buttonPressed(Button b) {
	}

	@Override
	public void buttonReleased(Button b) {
		if (b.equals(Button.ESCAPE)) {
			state = State.QUITTING;
			if (communication != null) {
				communication.sendSignal(RobotSignal.TURNING_OFF);
			}
		} 
	}

	public enum State {
		RUNNING,
		READY, 
		QUITTING;

		@Override 
		public String toString() {
			//only capitalize the first letter
			String s = super.toString();
			return s.substring(0, 1) + s.substring(1).toLowerCase();
		}
	}
}
