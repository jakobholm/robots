package predator;

import lejos.nxt.MotorPort;
import lejos.nxt.NXTRegulatedMotor;


/**
 * Basecode is copied from Ole Caprani's Car.java
 * 
 */
public class PredatorMover {
	private enum DrivingMode {
		FORWARD(1),
		BACKWARD(2),
		STOP(3);
		
		private final int value;
		
		DrivingMode(int value) {
			this.value = value;
		}
		
		public int getValue() {
			return value;
		}
	}
			

	private MotorPort leftMotor = MotorPort.B;
	private MotorPort rightMotor= MotorPort.C;
	private NXTRegulatedMotor hammer = new NXTRegulatedMotor(MotorPort.A);

	private final int power = 80;
	private final int turningPowerLow = 60;
	private final  int turningPowerHigh = 90;
	
	public boolean performingAttack = false;

	
	public PredatorMover() {}

	
//	private void drive() {
//		int leftPower = power, rightPower = power;
//		Status status = currentStatus;
//		
//		if (currentDirection == Direction.LEFT && currentStatus == Status.STOP) {
//			leftPower = 0;
//			rightPower = turningPowerHigh;
//			status = Status.FORWARD;
//		} else if (currentDirection == Direction.RIGHT && currentStatus == Status.STOP) {
//			leftPower = turningPowerHigh;
//			rightPower = 0;
//			status = Status.FORWARD;
//		} else if (currentDirection == Direction.LEFT) {
//			leftPower = turningPowerLow;
//			rightPower = turningPowerHigh;
//		} else if (currentDirection == Direction.RIGHT) {
//			rightPower = turningPowerLow;
//			leftPower = turningPowerHigh;
//		}
//		
//		leftMotor.controlMotor(leftPower, status.getValue());
//		rightMotor.controlMotor(rightPower, status.getValue());
//	}
	
	
	public void attack() {
		if (!performingAttack) { 
			performingAttack = true;
			new HammerThread(hammer, this);
		}
	}
	
	public void attackFinished() {
		performingAttack = false;
	}
	
	public void stopMoving() {
		leftMotor.controlMotor(0, DrivingMode.STOP.getValue());
		rightMotor.controlMotor(0, DrivingMode.STOP.getValue());

//		hammer.setSpeed(130);

		while (performingAttack) {
			// wait
		}
		if (hammer.getPosition() != 0) {
			performingAttack = true;
			new HammerThread(hammer, this);
		}
	}
	

	public void forward() {
		leftMotor.controlMotor(power, DrivingMode.FORWARD.getValue());
		rightMotor.controlMotor(power, DrivingMode.FORWARD.getValue());
	}

	public void backward() {
		leftMotor.controlMotor(power, DrivingMode.BACKWARD.getValue());
		rightMotor.controlMotor(power, DrivingMode.BACKWARD.getValue());
	}

	public void backwardLeft() {
		leftMotor.controlMotor(turningPowerLow, DrivingMode.BACKWARD.getValue());
		rightMotor.controlMotor(turningPowerHigh, DrivingMode.BACKWARD.getValue());
	}

	public void backwardRight() {
		leftMotor.controlMotor(turningPowerHigh, DrivingMode.BACKWARD.getValue());
		rightMotor.controlMotor(turningPowerLow, DrivingMode.BACKWARD.getValue());
	}

	public void forwardLeft() {
		leftMotor.controlMotor(turningPowerLow, DrivingMode.FORWARD.getValue());
		rightMotor.controlMotor(turningPowerHigh, DrivingMode.FORWARD.getValue());
	}

	public void forwardRight() {
		leftMotor.controlMotor(turningPowerHigh, DrivingMode.FORWARD.getValue());
		rightMotor.controlMotor(turningPowerLow, DrivingMode.FORWARD.getValue());
	}

	public void turnLeft() {
		leftMotor.controlMotor(0, DrivingMode.FORWARD.getValue());
		rightMotor.controlMotor(turningPowerHigh, DrivingMode.FORWARD.getValue());
	}

	public void turnRight() {
		leftMotor.controlMotor(turningPowerHigh, DrivingMode.FORWARD.getValue());
		rightMotor.controlMotor(0, DrivingMode.FORWARD.getValue());
	}
}
