package predator;
import lejos.nxt.NXTRegulatedMotor;

public class HammerThread extends Thread {
	private NXTRegulatedMotor hammerMoter;
	private PredatorMover mover;
	
	private int hammerForwardPostion = 110;
	private int hammerDefaultPostion = 0;
	private int hitTime = 900;
	private int stallTime = 600;	// Must be smaller than hitTime (approx 300 smaller)
	private int errorMargin = 5;
	
	public HammerThread(NXTRegulatedMotor hammerMoter, PredatorMover mover) {
		this.hammerMoter = hammerMoter;
		this.mover = mover;
		this.start();
	}

	@Override
	public void run() {
		if (hammerMoter.getPosition() == 0) {
			// move the hammer to attack, and then back to default position
			moveHammer(hammerForwardPostion, hammerDefaultPostion);
		} else {
			// if the hammer is not at default position when trying to attack, move it there
			moveHammer(hammerDefaultPostion, -1);
		}
		
		// signal to the mover that the attack is finished
		mover.attackFinished();
	}
	
	public void moveHammer(int firstPos, int secondPos) {
		hammerMoter.setStallThreshold(errorMargin, stallTime);
		
		// move hammer to firstPos
		hammerMoter.rotateTo(firstPos, true);
		
		// check if hammer is stalled
		int checkInterval = 10; // in milliseconds
		for(int i = 0; i < hitTime/checkInterval; i++) {	
			if (hammerMoter.isStalled() && hammerMoter.getPosition() != firstPos) {
				hammerMoter.suspendRegulation();
				return;
			}
			try { Thread.sleep(checkInterval); } catch (InterruptedException e) { }
		}
		
		// move the hammer to a potential second position.
		// this usually just means: move the hammer back to default. 
		if (secondPos != -1) {
			moveHammer(secondPos, -1);
		}
 	}
}