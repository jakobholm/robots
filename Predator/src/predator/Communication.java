package predator;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import lejos.nxt.LCD;
import lejos.nxt.Sound;
import lejos.nxt.comm.BTConnection;
import lejos.nxt.comm.Bluetooth;
import predator.Signal.PCSignal;
import predator.Signal.RobotSignal;

public class Communication {

	private InputStream dataIn;
	private OutputStream dataOut;
	private BTConnection connection;

	private boolean connected = false;
	private boolean handshaken = false;
	
	public Communication() {
		connect();
		Sound.playTone(256, 100, 50);
		try { Thread.sleep(500); } catch (InterruptedException e) { }
		LCD.clear();
	}

	private boolean connect() {
		// repeat this for the odd chance that someone else tries to connect.
		while (true) {
			LCD.clear();
			
			// connect and handshake
			connected = establishConnection();
			handshaken = handshake();
			
			if (isConnected()) {
				return true;
			} else {
				// reset connection
				connection.close();
				connected = handshaken = false; 
				try { Thread.sleep(500); } catch (InterruptedException e) {}
			}
		}
	}

	private boolean isConnected() {
		return connected && handshaken;
	}

	private boolean establishConnection() {
		LCD.drawString("Searching       ", 0, 0);
		connection = Bluetooth.waitForConnection(); // this method is very patient.
		LCD.drawString("Connected          ", 0, 1);
		return true;
	}

	private boolean handshake() {
		LCD.drawString("Start handshake       ", 0, 2);
		dataIn = connection.openDataInputStream();
		dataOut = connection.openDataOutputStream();
		
		// receive hello
		byte[] hello = receive(3);
		
		// check that hello is correct
		if (hello[0] != 'R' || hello[1] != 'C' || (hello[2] != 'P')) {
			LCD.drawString("WRONG HANDSHAKE         ", 0, 3);
			return false;
		}
			
		// send back hello
		send(hello);

		LCD.drawString("Handshake done      ", 0, 3);
		return true;
	}

	public PCSignal receiveSignal() {
		byte[] b = receive(1);
		return PCSignal.intToSignal((int) b[0]);
	}

	private byte[] receive(int size) {
		byte[] b = new byte[size];
		try {
			dataIn.read(b, 0, b.length);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return b;
	}

	protected void send(byte[] b) {
		try {
			dataOut.write(b, 0, b.length);
			dataOut.flush();
		} catch (IOException e) {
			System.out.println("send problem " + e);
		}
	}

	public void sendSignal(RobotSignal signal) {
		byte[] b = {(byte) signal.getValue()};
		send(b);
	}
}